# Camagru

## Live Demo
Camagru is available online: [Visit the site](https://camagru.mlefevre.xyz/)

## Overview
Camagru is a social media web application built using **Vanilla PHP** and **Vanilla JavaScript**. It was developed as part of the [Campus 19](https://campus19.be/) project, with security in mind, though it currently lacks monitoring, spam prevention, and bot mitigation features.

### Features
- User authentication and email notifications
- Profile management
- Comments and likes system
- Photo editor with some image processing
- Infinite scrolling post feed
- Theme customization
- Continuous Integration/Continuous Deployment (CI/CD) pipeline
- Emoji support
- Kubernetes support

## Screenshots
![Login Page](https://gitlab.com/BeBel42/camagru/-/raw/main/screenshots/home.png)
![Post Feed](https://gitlab.com/BeBel42/camagru/-/raw/main/screenshots/index.png)
![Photo Editor](https://gitlab.com/BeBel42/camagru/-/raw/main/screenshots/editor.png)

## Technologies Used
- **Backend:** PHP (pdo_mysql, php-gd)
- **Frontend:** JavaScript, SASS, Bootstrap
- **Database:** MariaDB (MySQL compatible)
- **Deployment:** Gitlab, Docker

## Installation & Setup

### Dependencies
Ensure the following dependencies are installed before proceeding:
- **PHP Extensions:** `pdo_mysql`, `php-gd`
- **SASS Compiler:** `sassc`

### Database Setup
You can run a local DB instance using the `dockerfile.yaml` file.
Run the following command to create the necessary database tables:
```sh
php -c php.ini setup.php
```

### Environment Variables
Create a `.env` file in the project root with the following configuration:
```sh
MARIADB_HOST=127.0.0.1
MARIADB_PORT=3306
MARIADB_DATABASE=camagru
MARIADB_USER=root
MARIADB_ROOT_PASSWORD=***

# SMTP Credentials
AuthPass=***
```

### Launch local instance
Once the .env and the DB are setup, you can launch the local instance:

``` shell
./launch.bash # launch local php server
```
``` shell
./sass-watch.bash # watch for scss files and compile them
```

## Kubernetes Compatibility
Camagru stores all its data in a **MariaDB** database, making it fully compatible with Kubernetes deployments.

## Contributing
If you’d like to contribute, feel free to fork the repository and submit a pull request. Feedback and suggestions are always welcome!

## License
This project is open-source and distributed under the **GNU AGPLv3 License**.

---

For more details, visit the official repository: [GitLab - Camagru](https://gitlab.com/BeBel42/camagru)
