<?php

require_once($_SERVER['DOCUMENT_ROOT'] . '/../src/php/session.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/../src/php/db.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/../src/php/layout.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/../src/php/dark-reader.php');

if (isset($_GET['uid'])) {
	try {
		$db = connectDb();
		cleanUids($db);
		if (!isUidValid($db, $_GET['uid'], 'password_reset'))
			throw new Exception('Link does not exist or is outdated');
		$newPwd = updatePassword($db, $_GET['uid']);
	} catch (Exception $e) {
		$_SESSION['alert-error'] = $e->getMessage();
	} finally {
		// login page will display verification success
		$_SESSION['alert-success'] = "
Your password has been reset to <strong>$newPwd</strong>
		";
		header('Location: /login');
		exit();
	}
}


$error = '';

$askForEmail = !isset($_POST['email']);
if (!$askForEmail) {
	try {
		$db = connectDb();
		$user = getUserByEmail($db, $_POST['email']);
		$email = $user['email'];
		$username = $user['username'];
		cleanUids($db);
		if (alreadySent($db, $email, 'password_reset'))
			throw new Exception(
				"Email was already sent.<br>"
					. "Please allow up to 5 minutes before requesting a new one"
			);
		$uid = setEmailId($db, $email, 'password_reset');

		$link = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]"
			. '?uid=' . $uid;

		$subject = "Hi " . htmlspecialchars($username)
			. ", here is a link to reset your password";
		$message = "<html><body>"
			. "Hi " . htmlspecialchars($username) . ", <br/>"
			. "you can reset your password by clicking the link below:<br/>"
			. "<a href='{$link}'>Reset my password</a><br/><br/>"
			. "<footer><i>Camagru</i></footer>"
			. "</body></html>";

		// To send HTML mail, the Content-type header must be set
		$headers = "From: " . 'Camagru' . " <" . 'noreply@mlefevre.xyz' . ">\r\n";
		$headers .= "Reply-To: " . 'noreply@mlefevre.xyz' . "\r\n";
		$headers .= "MIME-Version: 1.0\r\n";
		$headers .= "Content-type: text/html; charset=utf-8";

		if (!mail($email, $subject, $message, $headers))
			throw new Exception("failed to send mail to "
				. "<strong>{htmlspecialchars($email)}</strong>");
	} catch (Exception $e) {
		$error = $e->getMessage();
	}
}

?>

<!DOCTYPE html>
<html>

<head>
	<link rel="stylesheet" href="/styles/main.css">

	<?php include($_SERVER['DOCUMENT_ROOT'] . '/../src/php/metadata.php'); ?>
</head>

<body>
	<?php Layout\printHeader(); ?>
	<div class='container bg-light my-5 p-5 rounded shadow'>

		<div class='row mt-4 mb-5'>
			<h2 class='col text-center'>
				Reset your password
			</h2>
		</div>
		<?php if (!$askForEmail && !$error) : ?>
			<div class='row'>
				<div class='col text-center'>
					An email was sent to
					<strong><?= htmlspecialchars($email) ?></strong>
				</div>
			</div>
			<div class='row'>
				<div class='col text-center'>
					Not receiving the email? <a href="">Send a new one</a>
				</div>
			</div>
		<?php else : ?>
			<div class='row d-flex justify-content-center'>
				<div class='col-lg-6'>
					<form method="post">
						<label for='email'> Enter your email:</label>
						<input
							class='mb-2 px-4 py-2'
							type='email' name='email' id='email' required />
						<button title='Submit email' class='p-2' type="submit">Submit</button>
					</form>
				</div>
			</div>
		<?php endif; ?>
		<?php if ($error) : ?>
			<div class='row'>
				<div class='col alert-red-center'> <?= $error ?> </div>
			</div>
		<?php endif; ?>
		<div class='row mt-4'>
			<div class='col alert text-center mb-0'>
				A link will be sent to the provided email address. <br>
				Once the link clicked, you'll get a new password.
			</div>
		</div>
	</div>
	<br />
	<div class="container">
		<?php Layout\printFooter(); ?>
		</printFooter>
</body>

<?php Bonus\includeDR(); ?>

</html>
