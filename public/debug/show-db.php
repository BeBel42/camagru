<?php

require_once($_SERVER['DOCUMENT_ROOT'] . '/../src/php/db.php');

try {
    $db = connectDb();
    $tables = ["users", "posts", "sent_emails", "email_types", "cookie_tokens", "sessions", "pictures", "post_likes", "post_comments"];

    foreach ($tables as $table) {
        $query = "SELECT * FROM $table;";
        if ($table === 'pictures')
            $query = "SELECT id, waiting_for FROM $table;";
        echo('<h3> ' . $query . '</h3>');
        $results = $db->query($query);

        echo '<table border="1">';
        echo '<tr>';
        $firstRow = $results->fetch(PDO::FETCH_ASSOC);
        if ($firstRow) {
            foreach ($firstRow as $key => $value) {
                echo '<th>' . $key . '</th>';
            }
            echo '</tr>';
            do {
                echo '<tr>';
                foreach ($firstRow as $key => $value) {
                    if ($key === 'data') {
                        $value = strlen($value);
                    }
                    echo '<td>' . $value . '</td>';
                }
                echo '</tr>';
            } while ($firstRow = $results->fetch(PDO::FETCH_ASSOC));
        } else {
            echo '<tr><td colspan="2">No data found</td></tr>';
        }
        echo '</table>';
    }

} catch (Exception $e) {
    echo "Error: {$e->getMessage()}";
}

?>
