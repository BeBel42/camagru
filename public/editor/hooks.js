'use strict';

let imageIndex = 0; // first sticker index
const selectedImages = new Set(); // selected stickers

// delete snapshot (cross button)
async function deleteBase(id) {
	const thumbnailList = document.querySelector("#thumbnail-list");
	await fetch(`/api/delete-waiting-pic.php?id=${id}`)
		.then(response => {
			if (!response.ok)
				throw new Error('Network error');
		}).catch(error => console.error(error));
	await fetchThumbnails();
	if (thumbnailList.children.length < 6)
		enableSubmit();
}

// snapshot button pressed
async function snapshot() {
	const video = document.querySelector('#video-player');
	if (!video) return;
	const canvas = document.createElement("canvas");
	canvas.width = video.videoWidth;
	canvas.height = video.videoHeight;
	const context = canvas.getContext("2d");
	context.drawImage(video, 0, 0, video.videoWidth, video.videoHeight);
	const imageData = canvas.toDataURL('image/png');
	await submitImage(imageData, selectedImages);
}

// browse button pressed
function browse() {
	// creating fake input to press it immeditaly
	const input = document.createElement('input');
	input.type = 'file';
	input.accept = 'image/png, image/jpeg';
	input.onchange = (e) => {
		// getting file ref
		const file = e.target.files[0];
		// reading file
		const reader = new FileReader();
		reader.readAsDataURL(file); // read as data URL
		reader.onload = (readerEvent) => {
			const imageData = readerEvent.target.result;
			submitImage(imageData, selectedImages);
		};
	};
	input.click();
}

// scroll image list
async function nextImage() {
	imageIndex++;
	await fetchImages(imageIndex).catch(() => imageIndex--);
	tagImages();
}
async function prevImage() {
	imageIndex--;
	await fetchImages(imageIndex).catch(() => imageIndex++);
	tagImages();
}

// select / deselect img
function toggleImg(id) {
	const thumbnailList = document.querySelector("#thumbnail-list");
	const img = document.getElementById(id);
	const src = img.src;
	if (selectedImages.has(src))
		selectedImages.delete(src);
	else
		selectedImages.add(src);
	tagImages();
	if (thumbnailList.children.length >= 6)
		disableSubmit();
	else
		enableSubmit();

}
