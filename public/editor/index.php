<?php

require_once($_SERVER['DOCUMENT_ROOT'] . '/../src/php/session.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/../src/php/check_logged.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/../src/php/layout.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/../src/php/dark-reader.php');

$empty_img_url = 'https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Fwww.namepros.com%2Fattachments%2Fempty-png.89209%2F&f=1&nofb=1&ipt=a7dbbc32dd8917fe63ee7601d2b5a6ad9ed8394661b66d346b44e506e8ec7d44&ipo=images';

?>

<!doctype html>

<html>

<head>
	<link rel="stylesheet" href="/styles/main.css">

	<!-- Icons -->
	<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.10.5/font/bootstrap-icons.css">

	<?php include($_SERVER['DOCUMENT_ROOT'] . '/../src/php/metadata.php'); ?>
</head>

<body style='height: 100vh;'>
	<?php Layout\printHeader(true); ?>
	<div class="container">
		<div class='row'>
			<div hidden id='editor-error' class='col-12 alert-red-center'></div>
		</div>

		<div class='row'>
			<h1 class='col-12 text-center mt-5'>

				<i class='bi bi-tools me-1'></i>
				Image Editor
			</h1>
		</div>
		<div class='bg-light shadow p-5 mx-3 my-5'>
			<div class='row mt-3'>
				<div style='display: flex; align-items: center; justify-content: center;' class='col-9 p-0'>
					<div class='p-0'>
						<div class='d-flex align-items-center justify-content-center mb-4 gap-1'>
							<button title='Create image from webcam' id='snapshot-btn' class='add-base' onclick="snapshot()">Take Snapshot</button>
							<div class='text-center ms-2 me-2'> Or</div>
							<button title='Import image from computer' class='add-base' onclick='browse()'>Browse</button>
						</div>
						<video id="video-player" muted autoplay style='max-width: 100%;'>
							Please use a browser that supports HTML5 video
						</video>
					</div>
				</div>
				<div id='images-list' class='col-2 offset-1 pt-5 pe-5 mb-5'>

					<div class="tooltip-container">
						<i class="bi bi-info-circle h5"></i>
						<span class="tooltip-text">Images below are optional stickers that you can add on top of the image</span>
					</div>
					<script>
						// tooltip Javascript logic
						document.addEventListener("DOMContentLoaded", function() {
							const tooltipContainers = document.querySelectorAll(".tooltip-container");

							tooltipContainers.forEach(container => {
								container.addEventListener("click", function(event) {
									// Close any other open tooltips
									document.querySelectorAll(".tooltip-container").forEach(el => {
										if (el !== container) el.classList.remove("active");
									});

									// Toggle the tooltip for the clicked element
									container.classList.toggle("active");

									// Close tooltip when clicking outside
									document.addEventListener("click", function closeTooltip(e) {
										if (!container.contains(e.target)) {
											container.classList.remove("active");
											document.removeEventListener("click", closeTooltip);
										}
									});
								});
							});
						});
					</script>

					<button class='p-3' onclick='prevImage()'>
						<img
							title='Move image list upwards'
							alt='previous image button'
							height='40px'
							src='/images/up-arrow.png' />
					</button>
					<?php for ($i = 0; $i < 4; $i++) : ?>
						<?php $id = 'img-' . $i ?>
						<img
							alt='sticker image'
							onclick='toggleImg("<?= $id ?>")'
							height='70px'
							src='<?= $empty_img_url ?>' id='<?= $id ?>' />
					<?php endfor ?>
					<button class='p-3' onclick='nextImage()'>
						<img
							title='Move image list downwards'
							alt='next image button'
							height='40px'
							src='/images/up-arrow.png' class='invert-img' />
					</button>
				</div>
			</div>
			<div class='row my-5 mx-lg-5'>
				<div class='col-12'>
					<div id='thumbnail-list'>
					</div>
				</div>
			</div>
		</div>
		<br />
		<div class='row'>
			<?php Layout\printFooter(true); ?>
		</div>
	</div>
</body>

<script type="text/javascript" src="/editor/lib.js"></script>
<script type="text/javascript" src="/editor/hooks.js"></script>
<script type="text/javascript" src="/editor/startup.js"></script>
<?php Bonus\includeDR(); ?>

</html>
