<?php

require_once($_SERVER['DOCUMENT_ROOT'] . '/../src/php/session.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/../src/php/check_logged.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/../src/php/db.php');

// to make code shorter in case of error
function exit_err(string $print, int $code = 400): void {
	http_response_code($code);
	exit($print);
}

// get biggest dims that fits into image
function get_biggest_possible(int $bx, int $by, int $ox, int $oy): array
{
	$ratio = $bx / $ox;
	if ($ratio * $oy <= $by)
		return [
			'x' => $ratio * $ox,
			'y' => $ratio * $oy
		];
	$ratio = $by / $oy;
	return [
		'x' => $ratio * $ox,
		'y' => $ratio * $oy
	];
}

// applies an overlay to base
function add_overlay(GdImage $base_img, GdImage $overlay): void
{
	// get overlay dims
	$dims = get_biggest_possible(
		imagesx($base_img),
		imagesy($base_img),
		imagesx($overlay),
		imagesy($overlay)
	);

	// modifying dims
	define('divider', 3);
	$x_dim = $dims['x'] / divider;
	$y_dim = $dims['y'] / divider;

	// positioning
	$x_pos = random_int(0, (int)(imagesx($base_img) - $x_dim));
	$y_pos = random_int(0, (int)(imagesy($base_img) - $y_dim));

	// applying overlay to base
	imagecopyresampled(
		$base_img,
		$overlay,
		$x_pos,
		$y_pos,
		0,
		0,
		$x_dim,
		$y_dim,
		imagesx($overlay),
		imagesy($overlay),
	);
}

// converts a GdImage to a byte string
function GdImage_to_string(GdImage $image): string {
	ob_start();
	ob_clean();
	imagepng($image);
	$img_res = ob_get_contents();
	ob_end_clean();
	return $img_res;
}

// removing header of byte64
$img_b_64 = str_replace([
	'data:image/png;base64,',
	'data:image/jpeg;base64,',
], '', $_POST['image']);

// decoding and checking validity of file
if (($img_b = base64_decode($img_b_64)) === false)
	exit_err('Invalid base64 format');

// converting base image to GD object
$base_img = imagecreatefromstring($img_b);
if ($base_img === false)
	exit_err('File is not an image');

$public_dir = $_SERVER['DOCUMENT_ROOT'];

// applying overlays to base img
$overlay_list = json_decode($_POST['overlays']);
foreach ($overlay_list as $overlay_src) {
	$arr = [];
	preg_match('#/(?!.*/).+?$#', $overlay_src, $arr);
	$overlay_path = $public_dir . '/images' . $arr[0];
	$overlay = imagecreatefrompng($overlay_path);
	if ($overlay === false)
		exit_err('Overlay is not a png');
	add_overlay($base_img, $overlay);
 }

$user_id = (int)$_SESSION['logged_user'];

// upload file to db. Return nothing
try {
	$db = connectDb();
	createWaitingPicture($db, $user_id, GdImage_to_string($base_img));
} catch (Exception $e) {
	http_response_code(500);
	$error = $e->getMessage();
}

// db error
if (isset($error))
	exit_err($error, 500);
