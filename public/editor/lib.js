'use strict';

// fetches image and updates its source
async function fetchImages(index) {
	await fetch(`/api/get-images.php?index=${index}`)
		.then(r => r.json())
		.then(r => {
			r.forEach((name, i) => {
				document.getElementById(`img-${i}`).src = `/images/${name}`;
			})
		});
}

// make submit buttons available/unavailable
function disableSubmit() {
	document.querySelectorAll('.add-base').forEach(e =>
		e.setAttribute("disabled", "disabled"));
}
function enableSubmit() {
	document.querySelectorAll('.add-base').forEach(e =>
		e.removeAttribute("disabled"));
}

// publish finished image to post (redirects user)
async function publish(id) {
	window.location = '/publish?id=' + id;
}

// Adds thumbnail to dom
function addThumbnail(id) {
	const thumbnailList = document.querySelector("#thumbnail-list");
	const nDiv = document.createElement("div"); // parent div
	nDiv.style.position = 'relative';

	const nThumbnail = document.createElement("img"); // image
	const closeButton = document.createElement("button"); // cross button
	closeButton.onclick = () => deleteBase(id);
	closeButton.classList.add('btn-close',  'rounded-circle', 'p-2', 'h5');
	closeButton.title = 'Delete image';
	const publishButton = document.createElement("button"); // cross button
	publishButton.onclick = () => publish(id);
	publishButton.classList.add('btn', 'btn-primary', 'btn-sm',);
	publishButton.innerHTML = "Publish";
	publishButton.title = 'Publish image';

	nThumbnail.classList.add('point');
	nThumbnail.style.width = '100%';
	nThumbnail.src = `/api/get-waiting-pic.php?id=${id}&cache=${Math.random()}`;
	nThumbnail.addEventListener('click', () => window.open(nThumbnail.src));

	const btnDiv = document.createElement('div');
	btnDiv.append(publishButton, closeButton)
	btnDiv.classList.add('d-flex', 'justify-content-between', 'p-2');
	btnDiv.style.width = '100%';
	btnDiv.style.zIndex = '5';
	btnDiv.style.position = 'absolute';


	// inserting elems
	nDiv.prepend(nThumbnail);
	nDiv.prepend(btnDiv);
	thumbnailList.prepend(nDiv);
	// disabling buttons if already six canvas
	if (thumbnailList.children.length >= 6)
		disableSubmit();
}

// clear thumbnail list and add all thumbnails from user
async function fetchThumbnails() {
	document.querySelector("#thumbnail-list").innerHTML = '';
	const data = await fetch('/api/get-waiting-pics.php')
		.then(response => response.json())
	for (const id of data)
		addThumbnail(id);
}

// send image data to backend
async function submitImage(imageData, selectedImages) {
	const errorDiv = document.getElementById('editor-error');
	errorDiv.innerHTML = '';
	errorDiv.setAttribute('hidden', '');
	const formData = new FormData();
	formData.append('image', imageData);
	formData.append('overlays', JSON.stringify([...selectedImages]));
	try {
		const res = await fetch('/editor/submit.php', {
			method: 'POST',
			body: formData
		});
		if (!res.ok) {
			if (res.status !== 413) throw new Error(res.statusText);
			throw new Error('Image is too large. Must be 8M or smaller.');
		}
	} catch (error) {
		error = 'File upload failed: ' + error;
		errorDiv.innerHTML = error;
		errorDiv.removeAttribute('hidden');
	}
	await fetchThumbnails();
}

// color stickers according to selection
function tagImages() {
	for (let i = 0; i < 4; i++) {
		const id = `img-${i}`;
		const img = document.getElementById(id);
		const src = img.src;
		img.classList.remove('selected');
		if (selectedImages.has(src))
			img.classList.add('selected');
	}
}
