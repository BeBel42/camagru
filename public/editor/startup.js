'use strict';

// streaming webcam to video tag
window.navigator.mediaDevices.getUserMedia({
	video: true
}).then(stream => {
	const video = document.querySelector('#video-player');
	video.srcObject = stream;
}).catch((error) => {
	console.error(error);
	document.getElementById('snapshot-btn').classList.add('disabled');
	document.getElementById('video-player')
		.outerHTML = `
				<div class='p-5 mb-4 alert-red-center'>
					Permission to run webcam is needed for snapshots!
				</div>`;
});

// fetching first images
fetchImages(0);

// fetching all previous thumbnails of image
fetchThumbnails();
