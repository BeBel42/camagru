<?php

require_once($_SERVER['DOCUMENT_ROOT'] . '/../src/php/db.php');

// invalid image id - not found
if (!isset($_GET['id']) or
	filter_var($_GET['id'], FILTER_VALIDATE_INT) === false) {
	http_response_code(404);
	exit();
}

$pic_id = (int)$_GET['id'];

// Retrieve the image binary from the database and display it
try {
	$db = connectDb();
	$imageBinary = getPostPicture($db, $pic_id);
	header("Content-type: image/png");
	echo $imageBinary;
} catch (Exception $e) {
	http_response_code(404);
	$error = $e->getMessage();
}

if (isset($error))
	exit($error);

?>
