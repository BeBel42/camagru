<?php

function exit_400($message)
{
	// commecnted this because stupid 'no error' requirement
	// http_response_code(400);
	exit($message);
}

if (
	!isset($_GET['index'])
	|| filter_var($_GET['index'], FILTER_VALIDATE_INT) === false
)
	exit_400('Invalid or missing index');

$index = (int)$_GET['index'];

$image_path = $_SERVER['DOCUMENT_ROOT'] . '/images/';
$image_ls = `ls {$image_path}`;
$all_images = preg_split('/\s+/', $image_ls, -1, PREG_SPLIT_NO_EMPTY);

if ($index < 0 || $index + 4 > sizeof($all_images))
	exit_400('Index out of range');

$json_r = array_slice($all_images, $index, 4);

header('Content-Type: application/json; charset=utf-8');
echo json_encode($json_r, JSON_UNESCAPED_SLASHES);
