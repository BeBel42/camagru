<?php

require_once($_SERVER['DOCUMENT_ROOT'] . '/../src/php/session.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/../src/php/check_logged.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/../src/php/db.php');

function send_email(
	PDO $db,
	int $post_id,
	string $post_text,
	int $commenter_id
): void {
	$commenter = getUserById($db, $commenter_id);
	$post = get_post($db, $post_id);
	$username = $post['username'];
	$date = $post['date'];
	$email = $post['useremail'];
	$subject = "Hi " . htmlspecialchars($username)
		. ", someone commented your post";
	$img_src = 'http://' . $_SERVER['HTTP_HOST']
	. "/api/get-post-pic.php?id="
	. htmlspecialchars($post['picture']);
	$message = "<html><body>"
	. "Hi " . htmlspecialchars($username) . ", <br/>"
	. '<strong>' . htmlspecialchars($commenter['username'])
	. '@'
	. htmlspecialchars($commenter['email']) . '</strong>'
	. " has commented your post made on the "
	. htmlspecialchars($date)
	. "<br/><br/>"
	. htmlspecialchars($post_text) . '<br/><br/>'
	. '<img src="' . $img_src . '" />'
	. "<footer><i>Camagru</i></footer>"
	. "</body></html>";

	// To send HTML mail, the Content-type header must be set
	$headers = "From: " . 'Camagru' . " <" . 'noreply@mlefevre.xyz' . ">\r\n";
	$headers .= "Reply-To: " . 'noreply@mlefevre.xyz' . "\r\n";
	$headers .= "MIME-Version: 1.0\r\n";
	$headers .= "Content-type: text/html; charset=utf-8";

	if (!mail($email, $subject, $message, $headers))
		throw new Exception("failed to send mail to "
			. "<strong>" . htmlspecialchars($email) . "</strong>");
}

if (
    !isset($_POST['text']) ||
    $_POST['text'] === '' ||
    !isset($_POST['postId'])
	|| filter_var($_POST['postId'], FILTER_VALIDATE_INT) === false) {
	http_response_code(400);
	exit();
}

$post_id = (int) $_POST['postId'];
$user_id = (int) $_SESSION['logged_user'];
$post_text = $_POST['text'];

try {
	$db = connectDb();
	submit_comment($db, $user_id, $post_id, $post_text);
	$user = getUserById($db, $user_id);
	if ($user['comment_notify'] === 1)
		send_email($db, $post_id, $post_text, $user_id);
} catch (Exception $e) {
	http_response_code(400);
	$error = $e->getMessage();
}

if (isset($error))
	exit($error);
