<?php

require_once($_SERVER['DOCUMENT_ROOT'] . '/../src/php/session.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/../src/php/db.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/../src/php/time_elapsed_string.php');

$user_id = null;
$post_before = null;
$post = [];

$is_logged = !empty($_SESSION['logged_user']);

try {
	$db = connectDb();
	$post_user_id = null;
	if (isset($_GET['user_id'])) { // id of last post already on page
		if (filter_var($_GET['user_id'], FILTER_VALIDATE_INT) === false)
			throw new Exception('invalid user_id arg');
		$user_id = (int)$_GET['user_id'];
	}
	if (isset($_GET['before'])) {  // id of last post already on page
		if (filter_var($_GET['before'], FILTER_VALIDATE_INT) === false)
			throw new Exception('invalid before arg');
		$post_before = (int)$_GET['before'];
	}
	$post = get_post_before($db, $post_before, $user_id);
} catch (Exception $e) {
	$error = $e->getMessage();
}

// return nothing if no post
if (sizeof($post) === 0)
	exit();

?>



<div id='post-<?= htmlspecialchars($post['postid']) ?>' class='row d-flex justify-content-center mt-5'>
	<div class='col-md-12 col-xl-8'>
		<div class='card'>
			<div class='card-header' style='padding: 20px 20px'>
				<div class='card-title mx-4 my-2' style='display: flex; align-items: center; justify-content: space-between;'>
					<div>
						<a class='ms-0 me-0 fs-5' href='/?posts=<?= htmlspecialchars($post['userid']) ?>'>
							<?= htmlspecialchars($post['username']) ?></a>
						<span style='padding-left: 7px;'>
							<?= time_elapsed_string($post['date']) ?>
						</span>
					</div>
					<div>
						<?php if (
							$is_logged
							&& $post['userid'] === (int) $_SESSION['logged_user']
						) : ?>
							<button onclick='deletePost(<?= $post['postid'] ?>)' title='Delete post' class='btn-close rounded-circle p-2'></button>
						<?php endif; ?>
					</div>
				</div>
			</div>
			<div class='card-body p-0'>
				<img class='card-img' alt='post image content' src='
								<?= '/api/get-post-pic.php?id='
									. htmlspecialchars($post['picture']) ?>' />
			</div>
			<div class='card-footer px-3 py-5 px-md-5'>
				<div class='d-flex align-items-start gap-3 gap-md-5'>
					<?php if ($is_logged) : ?>
						<div title='Post likes' class='like-container mt-3' onclick="toggleLike(<?= htmlspecialchars($post['postid']) ?>)">
							<span id='likes-<?= htmlspecialchars($post['postid']) ?>'>0</span>
							<img alt='heart button' height='30' src='/images/heart.png' />
						</div>

						<div
							onfocusin='onCommentFocusIn(<?= $post["postid"] ?>)'
							onfocusout='onCommentFocusOut(<?= $post["postid"] ?>)'
							class='bg-white rounded' style='width: 100%;'>
							<textarea
								id='textarea-<?= $post["postid"] ?>'
								rows='2'
								oninput="getElementById('submit-comment-btn-<?= htmlspecialchars($post['postid']) ?>').disabled = this.value == ''"
								style='resize: none; border: 0' class='write-comment px-4 pb-2 pt-4' type='text' placeholder='Add a comment...'></textarea>
							<div
								onmousedown="event.preventDefault()"
								class='submit-comment submit-comment-invisible mx-2 mx-md-3 my-4'
								id='post-comment-btns-<?= $post["postid"] ?>'>


								<div class='me-1'>
									<button
										class='me-2 btn btn-outline-dark px-3 py-2' onclick='document.activeElement.blur()'>
										Cancel
									</button>
									<button
										id="submit-comment-btn-<?= htmlspecialchars($post['postid']) ?>"
										disabled
										class='btn btn-outline-primary px-3 py-2' onclick="submitComment(<?= htmlspecialchars($post['postid']) ?>)">
										Submit
									</button>
								</div>
							</div>
						</div>
					<?php else : ?>
						<div style='position: relative; width: 100%; display: flex; justify-content: start; align-items: center; padding-bottom: 15px;'>
							<div class='like-container mt-3' style='z-index: 5;' onclick='window.location = "/login"'>
								<span id='likes-<?= htmlspecialchars($post['postid']) ?>'>0</span>
								<img alt='heart button' height='30' src='/images/heart.png' />
							</div>
							<div style='position: absolute; width: 100%; display: flex; flex-direction: column; align-items: center; gap: 0.4em;'>
								<div>
									<a class='m-0 fs-4' href='/login'>Log in</a>
								</div>
								<div>
									To like and comment this post!
								</div>
							</div>
						</div>
					<?php endif; ?>
				</div>
				<div class='comment-section'>
					<div class='d-flex justify-content-center'>
						<button title='Load post comments' onclick="getComments(<?= htmlspecialchars($post['postid']) ?>)">
							Show Comments</button>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
