<?php

require_once($_SERVER['DOCUMENT_ROOT'] . '/../src/php/session.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/../src/php/check_logged.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/../src/php/db.php');

// input validation
if (
	!isset($_GET['postId'])
	|| filter_var($_GET['postId'], FILTER_VALIDATE_INT) === false
) {
	http_response_code(400);
	exit('Invalid postId');
}
if (
	!isset($_GET['commentId'])
	|| filter_var($_GET['commentId'], FILTER_VALIDATE_INT) === false
) {
	http_response_code(400);
	exit('Invalid commentId');
}
$post_id = (int) $_GET['postId'];
$comment_id = (int) $_GET['commentId'];
$user_id = (int) $_SESSION['logged_user'];

// getting comment list
try {
	$db = connectDb();
	$res = delete_comment($db, $post_id, $comment_id, $user_id);
} catch (Exception $e) {
	http_response_code(400);
	$error = $e->getMessage();
}

if (isset($error))
	exit($error);

