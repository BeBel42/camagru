<?php

require_once($_SERVER['DOCUMENT_ROOT'] . '/../src/php/session.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/../src/php/check_logged.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/../src/php/db.php');

if (
    !isset($_GET['theme'])
    || ($_GET['theme'] !== 'dark' && $_GET['theme'] !== 'white')) {
    http_response_code(400);
    exit();
}

$theme = $_GET['theme'];
$user_id = (int) $_SESSION['logged_user'];

try {
	$db = connectDb();
	set_theme($db, $user_id, $theme);
    $_SESSION['theme'] = $theme;
} catch (Exception $e) {
	http_response_code(400);
	$error = $e->getMessage();
}

if (isset($error))
	exit($error);
