<?php

require_once($_SERVER['DOCUMENT_ROOT'] . '/../src/php/session.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/../src/php/check_logged.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/../src/php/db.php');

if (!isset($_GET['id'])
	|| filter_var($_GET['id'], FILTER_VALIDATE_INT) === false) {
	http_response_code(404);
	exit();
}

$img_id = (int)$_GET['id'];
$user_id = (int)$_SESSION['logged_user'];

// Retrieve the image binary from the database and display it
try {
	$db = connectDb();
	$imageBinary = getWaitingPicture($db, $user_id, $img_id);
	header("Content-type: image/png");
	echo $imageBinary;
} catch (Exception $e) {
	http_response_code(404);
	$error = $e->getMessage();
}

if (isset($error))
	exit($error);
