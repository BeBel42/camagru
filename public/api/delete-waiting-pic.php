<?php

require_once($_SERVER['DOCUMENT_ROOT'] . '/../src/php/session.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/../src/php/check_logged.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/../src/php/db.php');

function exit_400($message) {
	http_response_code(400);
	exit($message);
}

if (!isset($_GET['id'])
	|| filter_var($_GET['id'], FILTER_VALIDATE_INT) === false)
	exit_400('Invalid or missing id');

$img_id = (int)$_GET['id'];
$user_id = (int)$_SESSION['logged_user'];

// Retrieve the image binary from the database and display it
try {
	$db = connectDb();
	$imageBinary = deleteWaitingPicture($db, $user_id, $img_id);
} catch (Exception $e) {
	http_response_code(500);
	$error = $e->getMessage();
}

if (isset($error))
	exit($error);

?>
