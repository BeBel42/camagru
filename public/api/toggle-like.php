<?php

require_once($_SERVER['DOCUMENT_ROOT'] . '/../src/php/session.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/../src/php/check_logged.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/../src/php/db.php');

if (!isset($_GET['postId'])
	|| filter_var($_GET['postId'], FILTER_VALIDATE_INT) === false) {
	http_response_code(400);
	exit();
}

$post_id = (int) $_GET['postId'];
$user_id = (int) $_SESSION['logged_user'];

try {
	$db = connectDb();
	toggle_like($db, $user_id, $post_id);
} catch (Exception $e) {
	http_response_code(400);
	$error = $e->getMessage();
}

if (isset($error))
	exit($error);
