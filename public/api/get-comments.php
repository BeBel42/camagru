<?php

require_once($_SERVER['DOCUMENT_ROOT'] . '/../src/php/session.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/../src/php/db.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/../src/php/time_elapsed_string.php');

// input validation
if (
	!isset($_GET['postId'])
	|| filter_var($_GET['postId'], FILTER_VALIDATE_INT) === false
) {
	http_response_code(400);
	exit('Invalid postId');
}

$is_logged = !empty($_SESSION['logged_user']);
$post_id = (int) $_GET['postId'];

// getting comment list
try {
	$db = connectDb();
	$res = get_comments($db, $post_id);
} catch (Exception $e) {
	http_response_code(400);
	$error = $e->getMessage();
}

if (isset($error))
	exit($error);

?>

<?php if (sizeof($res) === 0) : ?>
	<div class='text-center'>
		No comments were found. Be the first one to give your feedback!
	</div>
<?php else : ?>
	<?php foreach ($res as $comment) : ?>
		<div>
			<div>
				<a class='ms-0 me-0 fs-5' href='/?posts=<?= htmlspecialchars($comment['user_id']) ?>'>
					<?= htmlspecialchars($comment['username']) ?></a>

				<span style='margin: 0px 14px 0px 7px;'>
					<?= time_elapsed_string($comment['date']) ?>
				</span>
				<?php if (
					$is_logged
					&& $comment['user_id'] === (int)$_SESSION['logged_user']
				) : ?>
					<button title='Delete comment' onclick='deleteComment(<?= $comment['post_id'] ?>,
						<?= $comment['id'] ?>)' class='btn-close rounded-circle p-2'></button>
				<?php endif; ?>
			</div>
			<p><?= htmlspecialchars($comment['content']) ?></p>
		</div>
	<?php endforeach; ?>
<?php endif; ?>
