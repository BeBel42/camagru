<?php

require_once($_SERVER['DOCUMENT_ROOT'] . '/../src/php/session.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/../src/php/check_logged.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/../src/php/db.php');

$user_id = (int)$_SESSION['logged_user'];

// Retrieve the image binary from the database and display it
try {
	$db = connectDb();
	$list = getWaitingPictureList($db, $user_id);
	header('Content-Type: application/json; charset=utf-8');
	echo json_encode($list, JSON_UNESCAPED_SLASHES);
} catch (Exception $e) {
	http_response_code(500);
	$error = $e->getMessage();
}

if (isset($error))
	exit($error);

?>
