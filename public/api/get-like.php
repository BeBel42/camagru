<?php

require_once($_SERVER['DOCUMENT_ROOT'] . '/../src/php/session.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/../src/php/db.php');

if (!isset($_GET['postId'])
	|| filter_var($_GET['postId'], FILTER_VALIDATE_INT) === false) {
	http_response_code(400);
	exit();
}

$post_id = (int) $_GET['postId'];
$user_id = (int) ($_SESSION['logged_user'] ?? -1);

try {
	$db = connectDb();
	$json_r = get_like_status($db, $user_id, $post_id);
	header('Content-Type: application/json; charset=utf-8');
	echo json_encode($json_r);
} catch (Exception $e) {
	http_response_code(400);
	$error = $e->getMessage();
}

if (isset($error))
	exit($error);
