<?php

require_once($_SERVER['DOCUMENT_ROOT'] . '/../src/php/session.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/../src/php/check_logged.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/../src/php/db.php');

// input validation
if (
	!isset($_GET['postId'])
	|| filter_var($_GET['postId'], FILTER_VALIDATE_INT) === false
) {
	http_response_code(400);
	exit('Invalid postId');
}

$post_id = (int) $_GET['postId'];
$user_id = (int) $_SESSION['logged_user'];

// getting comment list
try {
	$db = connectDb();
	$res = delete_post($db, $post_id, $user_id);
	$_SESSION['alert-success'] = 'Your post has been deleted!';
} catch (Exception $e) {
	http_response_code(400);
	$error = $e->getMessage();
}

if (isset($error))
	exit($error);

