<?php

require_once($_SERVER['DOCUMENT_ROOT'] . '/../src/php/session.php');
if (!empty($_GET['theme']) && $_GET['theme'] === 'dark')
	$_SESSION['theme'] = 'dark';
require_once($_SERVER['DOCUMENT_ROOT'] . '/../src/php/db.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/../src/php/layout.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/../src/php/dark-reader.php');

$is_logged = !empty($_SESSION['logged_user']);

// if should login via cookie
if (!$is_logged && !empty($_COOKIE['token'])) {
	header('Location: /login');
	exit();
}

// replace 'current' by user id
if ($is_logged && isset($_GET['posts']) && $_GET['posts'] === 'current') {
	header('Location: /?posts=' . $_SESSION['logged_user']);
	exit();
}

// show popup and clean session variables
if (isset($_SESSION['alert-error']))
	$errorPopup = $_SESSION['alert-error'];
elseif (isset($_SESSION['alert-success']))
	$successPopup = $_SESSION['alert-success'];
unset($_SESSION['alert-success']);
unset($_SESSION['alert-error']);

$posts = [];

$title = 'Recent Posts';

try {
	$db = connectDb();
	$post_user_id = null;
	if (isset($_GET['posts'])) {
		if (filter_var($_GET['posts'], FILTER_VALIDATE_INT) === false)
			throw new Exception('invalid post id');
		$post_user_id = (int)$_GET['posts'];
		$username = getUserById($db, $post_user_id)['username'];
		$title = 'Posts by ' . $username;
	}
} catch (Exception $e) {
	$error = $e->getMessage();
}

?>

<!doctype html>

<html>

<head>
	<link rel="stylesheet" href="/styles/main.css">

	<!-- Icons -->
	<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.10.5/font/bootstrap-icons.css">

	<?php include($_SERVER['DOCUMENT_ROOT'] . '/../src/php/metadata.php'); ?>
</head>

<body>
	<?php Layout\printHeader($is_logged); ?>
	<div class="container">

		<?php if (isset($errorPopup)) : ?>
			<div class='row'>
				<div class='col-12 alert-red-center py-4 fs-5'>
					<?= $errorPopup ?>
				</div>
			</div>
		<?php endif; ?>
		<?php if (isset($successPopup)) : ?>
			<div class='row'>
				<div class='col-12 alert-green-center py-4 fs-5'>
					<?= $successPopup ?>
				</div>
			</div>
		<?php endif; ?>
		<?php if (isset($error)) : ?>
			<div class='row'>
				<div class='col-12 alert-red-center'> <?= $error ?> </div>
			</div>
		<?php endif; ?>
		<div class='row'>
			<h1 class='col-12 text-center mt-5'>
				<i class='bi bi-camera me-1'></i>
				<?= $title ?>
			</h1>
		</div>
		<div id='posts'>
			<!-- Posts fetched by JS will be put here -->
		</div>
		<div id='loading' class='d-flex justify-content-center'>
			<img
				alt='Loading...'
				class='turn' height='60' src='/images/loading.png' />
		</div>
		<div>
			<?php Layout\printFooter($is_logged); ?>
		</div>
	</div>
</body>

<script type="module" src="/index.js"></script>
<?php Bonus\includeDR(); ?>

</html>
