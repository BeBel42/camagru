import EmojiPicker from './EmojiPicker.js';

// fetches like info (count + color)
window.getLikeStatus = async function getLikeStatus(postId) {
	const res = await fetch('/api/get-like.php?postId=' + postId)
		.then(r => r.json());
	const heartImg = document.querySelectorAll(`#post-${postId} img`)[1];
	heartImg.classList.add('liked');
	if (res.isLiked === true)
		heartImg.src = '/images/heart.png'
	else
		heartImg.src = '/images/heart-outline.png'
	const likes = document.querySelector(`#likes-${postId}`);
	likes.innerHTML = res['likes'];
}

// like button pressed
window.toggleLike = async function toggleLike(postId) {
	await fetch('/api/toggle-like.php?postId=' + postId);
	await getLikeStatus(postId);
}

// fetches posts + returns in plain text
window.getNewPost = async function getNewPost(prevId, userId) {
	let path = '/api/get-next-post.php?';
	if (prevId !== undefined)
		path += '&before=' + prevId;
	if (userId !== undefined)
		path += '&user_id=' + userId;
	return fetch(path).then(r => r.text());
}

let stopFetching = false; // all posts have been fetched
let lock = false; // to avoid data race on multiple scrolls

// add new post to feed
window.addNewPost = async function addNewPost() {
	if (lock || stopFetching) return;
	lock = true;
	try {
		// getting params for fetch
		let userId = new URLSearchParams(window.location.search).get('posts');
		if (userId === null) userId = undefined;
		let prevId = document.querySelector('#posts').lastElementChild?.id;
		if (prevId !== undefined)
			prevId = prevId.split('-').pop();

		// fetching data
		let newPost = await getNewPost(prevId, userId);

		// no post left
		if (newPost === '') {
			stopFetching = true;
			document.querySelector('#loading').innerHTML = `
			<div class='row d-flex justify-content-center p-4'>
				<div class='col-12 col-md-8 text-center'>
					You've seen everything!
				</div>
			</div>`;
		}

		// update like button (count + color)
		document.querySelector('#posts').innerHTML += newPost;
		let newId = document.querySelector('#posts').lastElementChild?.id;
		if (newId) {
			newId = newId.split('-').pop();
			getLikeStatus(newId);
		}
	} catch (e) {
		throw e;
	} finally {
		lock = false;
	}
}

// is user near bottom of page
export function isAtBottom() {
	const scrollHeight = document.body.scrollHeight;
	const totalHeight = window.scrollY + window.innerHeight;
	const r = totalHeight >= scrollHeight;
	return r;
}

// fetch posts until fills screen
window.fetchFirstPosts = async function fetchFirstPosts() {
	while (!stopFetching && isAtBottom())
		await addNewPost()
}

fetchFirstPosts();

// load next posts when at bottom
window.onscroll = async () => {
	if (!stopFetching && isAtBottom())
		await addNewPost();
}

// submits + refresh comment section of post
window.submitComment = async function submitComment(postId) {
	const query = `#post-${postId} textarea.write-comment`;
	const textElem = document.querySelector(query);
	const text = textElem.value;
	if (!text) return; // empty - do nothing
	const formData = new FormData();
	formData.append('text', text);
	formData.append('postId', postId);
	await fetch('/api/submit-comment.php', {
		method: 'POST',
		body: formData
	});
	textElem.value = ''; /// empty textarea
	getComments(postId);
}

// fetches all comments of a post and overrides the comment section
// with its content
window.getComments = async function getComments(postId) {
	const r = await fetch('/api/get-comments.php?postId=' + postId)
		.then(r => r.text());
	document.querySelector(`#post-${postId} .comment-section`).innerHTML = r;
}

// cross btn next to comment
window.deleteComment = async function deleteComment(postId, commentId) {
	// delete comment from db
	await fetch(`/api/delete-comment.php?postId=${postId}&commentId=${commentId}`);
	// refresh comments
	await getComments(postId);
}

// cross btn next to comment
window.deletePost = async function deletePost(postId) {
	// delete comment from db
	await fetch(`/api/delete-post.php?postId=${postId}`);
	// refresh page
	location.reload();
}


// show button "upload comment" or hide it
window.onCommentFocusIn = function onCommentFocusIn(postId) {
	// make textarea buttons visible
	const postCommentBtns = document.getElementById(`post-comment-btns-${postId}`);

	// add emoji button in button list
	const emojiButton = document.createElement('div');
	emojiButton.classList.add('d-flex', 'justify-content', 'align-items-center');
	emojiButton.innerHTML = `<div data-emoji-picker="true"'></div>`;
	emojiButton.id = `emoji-btn-${postId}`;
	postCommentBtns.prepend(emojiButton)

	// set input as capable input for emoji
	const textArea = document.getElementById(`textarea-${postId}`);
	textArea.setAttribute('data-emoji-picker-input', 'true');

	// make button list visible
	postCommentBtns.classList.remove('submit-comment-invisible');

	// get all posts on page
	let allPosts = [...document.querySelectorAll(`[id^=post-`)].map(c => c.id);
	allPosts = allPosts.filter(p => p.split('-').length === 2);

	// focus out other comments that this one
	for (const post of allPosts) {
		const quitPostId = Number(post.split('-')[1]);
		if (quitPostId !== Number(postId)) {
			onCommentFocusOut(quitPostId);
		}

	}

	// init emoji picker for this input
	new EmojiPicker();
}
window.onCommentFocusOut = function onCommentFocusOut(postId) {
	// make textarea buttons invisible
	document.getElementById(`post-comment-btns-${postId}`).classList.add('submit-comment-invisible');

	// empty textarea
	const textArea = document.getElementById(`textarea-${postId}`);
	textArea.value = '';

	// not a candidate for emojis anymore
	textArea.removeAttribute('data-emoji-picker-input');

	// remove emoji button
	const emojiButton = document.getElementById(`emoji-btn-${postId}`);
	emojiButton?.remove();
}
