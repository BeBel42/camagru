<?php

require_once($_SERVER['DOCUMENT_ROOT'] . '/../src/php/session.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/../src/php/check_logged.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/../src/php/db.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/../src/php/layout.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/../src/php/dark-reader.php');

try {
	$db = connectDb();
	$user = getUserById($db, $_SESSION['logged_user']);
	if (!empty($_GET['logout'])) {
		$theme = 'white';
		if (!empty($_SESSION['theme'] && $_SESSION['theme'] === 'dark'))
			$theme = 'dark';
		deleteCookieToken($db, $user['id']);
		//unset cookies
		if (isset($_COOKIE['token'])) {
			unset($_COOKIE['token']);
			setcookie('token', '', time() - 3600, '/'); // empty value and old timestamp
		}
		session_destroy();
		header('Location: /?theme=' . $theme);
		exit();
	}
	if (!empty($_POST['current-password'])) {
		$curPwd = $_POST['current-password'];
		if (!password_verify($curPwd, $user['password']))
			throw new Exception('Invalid password');
		$newUser = $user;
		if (!empty($_POST['new-password']))
			$newUser['password'] = password_hash(
				$_POST['new-password'],
				PASSWORD_DEFAULT
			);
		if (!empty($_POST['new-email'])) {
			$newEmail = true;
			$newUser['email'] = $_POST['new-email'];
		}
		if (!empty($_POST['new-username']))
			$newUser['username'] = $_POST['new-username'];
		if (
			!empty($_POST['new-password'])
			&& !(strlen($_POST['new-password']) >= 8
				&& preg_match('/[A-Z]/', $_POST['new-password'])
				&& preg_match('/[a-z]/', $_POST['new-password']))
		)
			throw new Exception('Password should be at least 8 characters,
and both lower and upper case letters');
		if (!empty($_POST['new-email']))
			$_POST['new-email'] = filter_var(
				$_POST['new-email'],
				FILTER_SANITIZE_EMAIL
			);
		if (
			!empty($_POST['new-email'])
			&& !filter_var($newUser['email'], FILTER_VALIDATE_EMAIL)
		)
			throw new Exception('Invalid email');
		if (
			!empty($_POST['new-email'])
			&& !isEmailNew($db, $newUser['email'])
		)
			throw new Exception('A user with this email address already exists');
		updateUser($db, $newUser);
		$user = $newUser;
		$success = 'User credentials have been updated successfully!';
		if (isset($newEmail)) {
			unverifyEmail($db, $_SESSION['logged_user']);
			$_SESSION = [];
			$_SESSION['email'] = $user['email'];
			$_SESSION['username'] = $user['username'];
			$_SESSION['user_id'] = $user['id'];
			$_SESSION['theme'] = $user['theme'];
			$_SESSION['success'] = $success;
			deleteCookieToken($db, $user['id']);
			header('Location: /confirm-email');
			exit();
		}
	}
} catch (Exception $e) {
	$error = $e->getMessage();
}

$new_email_fill = htmlspecialchars($_POST['new-email'] ?? '');
$new_username_fill = htmlspecialchars($_POST['new-username'] ?? '');

?>

<!doctype html>

<html>

<head>
	<link rel="stylesheet" href="/styles/main.css">

	<!-- Icons -->
	<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.10.5/font/bootstrap-icons.css">

	<?php include($_SERVER['DOCUMENT_ROOT'] . '/../src/php/metadata.php'); ?>
</head>

<body>
	<?php Layout\printHeader(true); ?>
	<div class='row mt-4 mb-5'>
		<h1 class='col-12 text-center mt-5'>
			<i class='bi bi-gear me-1'></i>
			Settings
		</h1>
	</div>
	<div class="container bg-light p-3 mt-3 shadow rounded mt-4 mb-5 p-5">
		<div class='row mb-0'>
			<a class='col fs-2 ps-0' href='/?posts=<?= htmlspecialchars($user['id']) ?>'>
				<?= htmlspecialchars($user['username']) ?>
			</a>
		</div>
		<div class='row mb-4'>
			<h5 class='col ms-2'>
				<i><?= htmlspecialchars($user['email']) ?></i>
			</h5>
		</div>
		<div class='row mb-5'>
			<div class='col mb-3'>
				<div class='mb-1'>
					Email Notifications
				</div>
				<div>
					<button title='Toggle email notifications' style='width: 10em' onclick='toggleCommentNotify()' id='comm-not-btn' class='btn btn-outline-primary'>...
					</button>
				</div>
			</div>
			<div class='row'>
				<div class='col'>
					<div class='mb-1'>
						Theme
					</div>
					<div>
						<?php \Bonus\DRButton() ?>
					</div>
				</div>
			</div>
		</div>
		<?php if (isset($error)) : ?>
			<div class="row">
				<div class='col alert-red-center'> <?= $error ?> </div>
			</div>
		<?php endif; ?>
		<?php if (isset($success)) : ?>
			<div class="row">
				<div class='col alert-green-center'> <?= $success ?> </div>
			</div>
		<?php endif; ?>
		<form method="post">
			<h2>Update profile information</h2>
			<p>Enter here what you want to update.
				Omit the fields you want to keep.</p>
			<label for='c-pass'>Current password *</label>
			<input type="password"
				class='mb-2 px-4 py-2'
				name="current-password" id='c-pass' required />
			<label for='n-pass'>New password</label>
			<input
				class='mb-2 px-4 py-2'
				type="password" name="new-password" id='n-pass' />
			<label for='n-mail'>New email</label>
			<input
				class='mb-2 px-4 py-2'
				value='<?= $new_email_fill ?>' type="email" name="new-email" id='n-mail' />
			<label for='n-user'>New username</label>
			<input
				class='mb-2 px-4 py-2'
				value='<?= $new_username_fill ?>' type="text" name="new-username" id='n-user' />
			<button title='Submit changes' class='w-25 btn btn-primary btn-lg' type="submit">Submit</button>
			<div class='alert'>* Mandatory field</div>
		</form>
	</div>
	<br />
	<div class="container">
		<?php Layout\printFooter(true); ?>
	</div>
</body>

<?php \Bonus\includeDR() ?>
<script type="text/javascript" src="/settings/index.js"></script>

</html>
