'use strict';

// updates btn content according to db
async function getCommentNotify() {
	const btn = document.getElementById('comm-not-btn');
	btn.innerHTML = '...'
	const r = await fetch('/api/get-comment-notify.php').then(r => r.json());
	const onHTML = "<i class='bi bi-envelope-at-fill pe-1'></i> On";
	const offHTML = "<i class='bi bi-envelope-at pe-1'></i> Off";
	btn.innerHTML = r.value ? onHTML : offHTML;
	btn.classList.remove('btn-outline-primary', 'btn-primary');
	if (r.value)
		btn.classList.add('btn-primary');
	else
		btn.classList.add('btn-outline-primary');
}

// toggles comment notifications for users
async function toggleCommentNotify() {
	await fetch('/api/toggle-comment-notify.php');
	await getCommentNotify();
}


function toggleDR() {
	if (DarkReader.isEnabled()) {
		DarkReader.disable();
		fetch('/api/update-theme.php?theme=white');
	}
	else {
		DarkReader.enable({
			brightness: 100,
			contrast: 90,
			sepia: 10
		});
		fetch('/api/update-theme.php?theme=dark');
	}
	updateDRButton();
}

function updateDRButton() {
	const btn = document.getElementById('dr-btn');
	const darkHTML = "<i class='bi bi-moon pe-1'></i> Dark";
	const lightHTML = "<i class='bi bi-sun pe-1'></i> Light";
	if (btn)
		btn.innerHTML = (DarkReader.isEnabled() ? darkHTML : lightHTML);
}
updateDRButton();


getCommentNotify();
