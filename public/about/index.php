<?php

require_once($_SERVER['DOCUMENT_ROOT'] . '/../src/php/session.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/../src/php/layout.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/../src/php/dark-reader.php');

// shorthand array for links
const link_array = [
	'19' => 'https://campus19.be/nl/de-19-campussen/',
	'42' => 'https://www.42network.org/',
	'pdf' => 'https://gitlab.com/BeBel42/camagru/-/blob/main/en.subject.pdf?ref_type=heads',
	'DR' => 'https://github.com/darkreader/darkreader',
	'BS' => 'https://getbootstrap.com/',
	'sass' => 'https://sass-lang.com/',
	'art' => 'https://www.instagram.com/sadbooxx/',
];

// generate <a> from args
function l(string $name, string $key)
{
	$href = link_array[$key];
	return '<a class="m-0" href="' . $href . '">' . $name . '</a>';
}

$logged_in = isset($_SESSION['logged_user']);

?>

<!DOCTYPE html>
<html>

<head>
	<link rel="stylesheet" href="/styles/main.css">

	<?php include($_SERVER['DOCUMENT_ROOT'] . '/../src/php/metadata.php'); ?>
</head>

<body>
	<?php Layout\printHeader($logged_in); ?>
	<div class='container'>
		<div class='row m-4 justify-content-center'>
			<div class='col-lg-8 text-left p-5 rounded shadow my-5' style='background-color: #fafafa;'>
				<h1 class='text-center m-4 mb-5'> About Camagru </h1>
				<hr class='my-5' />
				<h2 class='m-3'>A student project</h2>
				<p class='m-3'>
					Camagru is a project from <?= l('Campus 19', '19') ?> (part of the <?= l('42 Network', '42') ?>). <br />
					I had to do this website alone, and I had to follow a <?= l('set of rules', 'pdf') ?>
					in order to obtain a good grade.
				</p>
				<p class='m-3'>
					Art of the Camagru Dragon is made by Maeva Latour
					(<?= l('@sadbooxx', 'art') ?>), an art student in Brussels.
				</p>
				<hr class='my-5' />
				<h2 class='m-3'>Technical details</h2>
				<p class='m-3'>
					This site is made in <b>vanilla PHP</b>, <b>vanilla JS</b>, <?= l('Bootstrap', 'BS') ?> and <?= l('SASS', 'sass') ?>. <br />
					It was firstly using <b>SQLite3</b>, then migrated to <b>Postgresql</b>,
					to get a stateless app (in order to support <b>Kubernetes</b> deployment).
					The session data is stored in the database and not the local
					file directory. <br />
					The dark theme is generated via <?= l('Dark Reader', 'DR') ?>.
				</p>
				<hr class='my-5' />
				<h2 class='m-3'>Security and Privacy</h2>
				<p class='m-3'>
					This website was made with security in mind.
					While it is not protected against bots and spam, the website
					has to <b>pass verification tests from three students of the school</b>,
					be it students that specialize in <b>Web</b>, <b>UNIX</b>, <b>Cybersecurity</b>, etc.
					No data is used in any way other than what you see on the website.
					The website is <b>self-hosted</b> (bare-metal). No data is stored on the cloud.
				</p>
			</div>
		</div>
		<?php Layout\printFooter($logged_in); ?>
	</div>
</body>

<?php Bonus\includeDR(); ?>

</html>
