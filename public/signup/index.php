<?php

require_once($_SERVER['DOCUMENT_ROOT'] . '/../src/php/session.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/../src/php/db.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/../src/php/layout.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/../src/php/dark-reader.php');

function redirectUser(string $email, string $userName, int $id): void
{
	$_SESSION['username'] = $userName;
	$_SESSION['email'] = $email;
	$_SESSION['user_id'] = $id;
	header("Location: /confirm-email");
	exit();
}

$has_submitted = $_SERVER['REQUEST_METHOD'] == 'POST';

// are all fields filled
$missing_fields = [];
$required = ['username', 'password', 'confirm-password', 'email'];
foreach ($required as $f_name)
	if (empty($_POST[$f_name]))
		array_push($missing_fields, $f_name);
$has_missing_fields = (bool)sizeof($missing_fields);

// input validation
$_POST['email'] = filter_var($_POST['email'] ?? '', FILTER_SANITIZE_EMAIL);
$passwordMatch = $has_submitted
	&& ($_POST['password'] === $_POST['confirm-password']);
$validEmail = $has_submitted
	&& filter_var($_POST['email'], FILTER_VALIDATE_EMAIL);
$error = '';
if ($has_submitted && !$validEmail)
	$error = 'Invalid Email.';
if ($has_submitted && !$passwordMatch)
	$error = 'Passwords do not match.';
if (
	$has_submitted
	&& !($passwordFullfills = strlen($_POST['password']) >= 8
		&& preg_match('/[A-Z]/', $_POST['password'])
		&& preg_match('/[a-z]/', $_POST['password']))
)
	$error = 'Password should be at least 8 characters,
and both lower and upper case letters';


// db requests if input is valid
$can_try_db =
	$has_submitted
	&& $validEmail
	&& $passwordMatch
	&& $passwordFullfills
	&& !$has_missing_fields;

if ($can_try_db) {
	try {
		$db = connectDb();
		if (!isEmailNew($db, $_POST['email']))
			throw new Exception('A user with this email address already exists');
		$id = createUser(
			$db,
			$_POST['username'],
			$_POST['password'],
			$_POST['email']
		);
		redirectUser($_POST['email'], $_POST['username'], $id);
	} catch (Exception $e) {
		$error = $e->getMessage();
	}
}

$username_fill = htmlspecialchars($_POST['username'] ?? '');
$email_fill = htmlspecialchars($_POST['email'] ?? '');

?>

<!DOCTYPE html>
<html>

<head>
	<link rel="stylesheet" href="/styles/main.css">

	<?php include($_SERVER['DOCUMENT_ROOT'] . '/../src/php/metadata.php'); ?>
</head>

<body>
	<?php Layout\printHeader(); ?>
	<div class="container">
		<?php if ($has_submitted && $has_missing_fields) : ?>
			<div class='row d-flex justify-content-center'>
				<div class='alert-red-center col-lg-10'>
					Some fields were omitted:
					<ul class='list-inline'>
						<?php foreach ($missing_fields as $n) : ?>
							<li><?= $n ?></li>
						<?php endforeach; ?>
					</ul>
				</div>
			</div>
		<?php elseif ($error) : ?>
			<div class='row d-flex justify-content-center'>
				<div class='alert-red-center col-lg-10'>
					<?= $error ?>
				</div>
			</div>
		<?php endif; ?>
		<div class="row d-flex justify-content-center my-4">
			<div id='box' class='col-lg-8 shadow my-5 bg-light' style='padding: 60px;'>
				<h1 class='text-left mb-4'>Sign Up</h1>
				<form method='post'>
					<label for="username-input">Username</label>
					<input
						value='<?= $username_fill ?>'
						name='username' id='username-input'
						class='mb-2 px-3 py-2'
						type="text" required />
					<label for="email-input">Email</label>
					<input
						value='<?= $email_fill ?>'
						class='mb-2 px-3 py-2'
						name='email' id='email-input' type="text" required />
					<label for="password-input">Password *</label>
					<input name='password'
						class='mb-2 px-3 py-2'
						id='password-input' type="password" required />
					<label for="confirm-password-input">Confirm Password</label>
					<input
						class='mb-2 px-3 py-2'
						name='confirm-password' id='confirm-password-input' type="password" required />
					<div class='alert alert-info mt-2'>
						* The password should be at least 8 characters long,
						and contain both lower and upper case letters
					</div>
					<button title='Submit sign up form' class='btn btn-primary btn-lg mt-4'>Submit</button>
				</form>
			</div>
		</div>
		<?php Layout\printFooter(); ?>
	</div>
</body>

<?php Bonus\includeDR(); ?>

</html>
