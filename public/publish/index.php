<?php

require_once($_SERVER['DOCUMENT_ROOT'] . '/../src/php/session.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/../src/php/check_logged.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/../src/php/db.php');

$user_id = (int)$_SESSION['logged_user'];

try {
	// input validation
	if (!isset($_GET['id']))
		throw new Exception('Missing id argument');
	if (filter_var($_GET['id'], FILTER_VALIDATE_INT) === false)
		throw new Exception('Invalid id');
	$id = (int)$_GET['id'];
	// submitting post (will do nothing in case of invalid id)
	$db = connectDb();
	submit_post($db, $user_id, $id);
	$_SESSION['alert-success'] = 'Post published successfully';
} catch (Exception $e) {
	$error = $e->getMessage();
}

if (isset($e))
	$_SESSION['alert-success'] = 'Could not publish post: ' . $error;

header('Location: /?posts=current');
