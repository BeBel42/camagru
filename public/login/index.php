<?php

require_once($_SERVER['DOCUMENT_ROOT'] . '/../src/php/session.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/../src/php/db.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/../src/php/layout.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/../src/php/dark-reader.php');

function tryCookie(): void
{
	if (empty($_COOKIE['token']))
		return;
	try {
		$db = connectDb();
		$user = getUserFromToken($db, $_COOKIE['token']);
		$token = setCookieToken($db, $user['id']);
		setcookie('token', $token, time() + (86400 * 365), "/", httponly: true, secure: true); // 1 year
		if (isset($_SESSION['alert-error']))
			$errorPopup = $_SESSION['alert-error'];
		elseif (isset($_SESSION['alert-success']))
			$successPopup = $_SESSION['alert-success'];
		$_SESSION = [];
		$_SESSION['logged_user'] = $user['id'];
		if (isset($errorPopup))
			$_SESSION['alert-error'] = $errorPopup;
		if (isset($successPopup))
			$_SESSION['alert-success'] = $successPopup;
		header('Location: /');
		exit();
	} catch (Exception) {
		return;
	}
}

tryCookie();

if (isset($_SESSION['alert-error']))
	$errorPopup = $_SESSION['alert-error'];
elseif (isset($_SESSION['alert-success']))
	$successPopup = $_SESSION['alert-success'];
unset($_SESSION['alert-success']);
unset($_SESSION['alert-error']);

$try_login = !(empty($_POST['password']) || empty($_POST['email']));

if ($try_login) {
	$password = $_POST['password'];
	$email = filter_var($_POST['email'], FILTER_SANITIZE_EMAIL);
	try {
		$db = connectDb();
		if (!isVerified($db, $email)) {
			$user_info = getUserByEmail($db, $email);
			$_SESSION['email'] = $user_info['email'];
			$_SESSION['username'] = $user_info['username'];
			$_SESSION['user_id'] = $user_info['id'];
			throw new Exception(
				'Your email has not been verified.<br/>'
					. "Don't have a link? "
					. '<a class="link-primary" href="/confirm-email">Confirm my email</a>'
			);
		}
		if (($id = logIn($db, $email, $password)) === false)
			throw new Exception('Invalid email or password.');
		$token = setCookieToken($db, $id);
		setcookie('token', $token, time() + (86400 * 365), "/", httponly: true, secure: true); // 1 year
		$_SESSION = [];
		$_SESSION['logged_user'] = $id;
		header('Location: /');
		exit();
	} catch (Exception $e) {
		$error = $e->getMessage();
	}
}

$email_fill = htmlspecialchars($_POST['email'] ?? '');

?>


<!DOCTYPE html>
<html>

<head>
	<link rel="stylesheet" href="/styles/main.css">

	<?php include($_SERVER['DOCUMENT_ROOT'] . '/../src/php/metadata.php'); ?>
</head>

<body>
	<?php Layout\printHeader(); ?>
	<div class='container'>
		<div class='row d-flex align-items-center my-5'>
			<div id='box' class='col-lg-4 bg-light shadow p-5'>
				<?php if (isset($errorPopup)) : ?>
					<div class='alert-red-center my-2'>
						<?= $errorPopup ?>
					</div>
				<?php endif; ?>
				<?php if (isset($successPopup)) : ?>
					<div class='alert-green-center my-2'>
						<?= $successPopup ?>
					</div>
				<?php endif; ?>
				<h1 id='title' class='text-center'> Log In </h1>
				<form method="post" class='my-4'>
					<labelfor="email">Email</label>
						<input class='mb-2 px-4 py-2' value='<?= $email_fill ?>' type="email" name="email" id='email' required />
						<label for="password">Password</label>
						<input class='mb-2 px-4 py-2' type="password" name="password" id='password' required />
						<button title='Submit log in form' class='btn btn-primary btn-lg my-4' type="submit">Submit</button>
				</form>
				<?php if (isset($error)) : ?>
					<div class='alert-red-center my-2'>
						<?= $error ?>
					</div>
				<?php endif; ?>
				<hr>
				<div class='d-flex flex-column justify-content-center align-items-center mt-4'>
					<div> Do not have an account? </div>
					<div class='my-2'>
						<a title='Go to sign up page' class='link-dark py-2 px-3 btn btn-primary btn-lg mt-2 mb-3' href="/signup">Sign up</a>
					</div>
					<div class='mt-2'> <a href='/password-reset'>I forgot my password</a> </div>
				</div>
			</div>

			<div class='col-lg-7 offset-1 m-md-4'>
				<img alt="Camagru's mascot"
					class='img-fluid' src='/images/mascot1.png' />
			</div>
		</div>
		<?php Layout\printFooter(); ?>
	</div>
</body>

<?php Bonus\includeDR(); ?>

</html>
