<?php

require_once($_SERVER['DOCUMENT_ROOT'] . '/../src/php/session.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/../src/php/db.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/../src/php/layout.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/../src/php/dark-reader.php');

if (isset($_GET['uid'])) {
	try {
		$db = connectDb();
		cleanUids($db);
		if (!isUidValid($db, $_GET['uid'], 'verification'))
			throw new Exception('Link does not exist or is outdated');
		verifyEmail($db, $_GET['uid']);
	} catch (Exception $e) {
		$_SESSION['alert-error'] = $e->getMessage();
	} finally {
		// login page will display verification success
		$_SESSION['alert-success'] = 'Your email has been verified';
		header('Location: /login');
		exit();
	}
}

if (
	!isset($_SESSION['email'])
	|| !isset($_SESSION['username'])
	|| !isset($_SESSION['user_id'])
)
	exit("Please come here via our <a href='/signup'>signup page</a>.");

$email = $_SESSION['email'];
$username = $_SESSION['username'];
$user_id = $_SESSION['user_id'];

if (isset($_SESSION['success']))
	$success = $_SESSION['success'];
unset($_SESSION['success']);

$error = '';

try {
	$db = connectDb();
	cleanUids($db);
	if (alreadySent($db, $email, 'verification'))
		throw new Exception(
			"Email was already sent.<br>"
				. "Please allow up to 5 minutes before requesting a new one"
		);
	$uid = setEmailId($db, $email, 'verification');

	$link = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]"
		. '?uid=' . $uid;

	$subject = "Hi " . htmlspecialchars($username)
		. ", please verify you email";
	$message = "<html><body>"
		. "Hi " . htmlspecialchars($username) . ", <br/>"
		. "please verify your email address by clicking the link below:<br/>"
		. "<a href='{$link}'>Verify your email address</a><br/><br/>"
		. "<footer><i>Camagru</i></footer>"
		. "</body></html>";

	// To send HTML mail, the Content-type header must be set
	$headers = "From: " . 'Camagru' . " <" . 'noreply@mlefevre.xyz' . ">\r\n";
	$headers .= "Reply-To: " . 'noreply@mlefevre.xyz' . "\r\n";
	$headers .= "MIME-Version: 1.0\r\n";
	$headers .= "Content-type: text/html; charset=utf-8";

	if (!mail($email, $subject, $message, $headers))
		throw new Exception("failed to send mail to "
			. "<strong>{htmlspecialchars($email)}</strong>");
} catch (Exception $e) {
	$error = $e->getMessage();
}

?>

<!DOCTYPE html>
<html>

<head>
	<link rel="stylesheet" href="/styles/main.css">

	<?php include($_SERVER['DOCUMENT_ROOT'] . '/../src/php/metadata.php'); ?>
</head>

<body>
	<?php Layout\printHeader(); ?>
	<div class='container p-3 mt-3'>
		<?php if (isset($success)) : ?>
			<div class="row">
				<div class='col alert-green-center'> <?= $success ?> </div>
			</div>
		<?php endif; ?>
		<div class='row'>
			<h3 class='col text-center'>
				Please verify your email address
			</h3>
		</div>
		<div class='row'>
			<div class='col text-center'>
				A verification link was sent to
				<strong><?= htmlspecialchars($email) ?></strong>
			</div>
		</div>
		<div class='row'>
			<div class='col text-center'>
				Not receiving the email? <a href="">Send a new one</a>
			</div>
		</div>
		<?php if ($error) : ?>
			<div class='row'>
				<div class='col alert-red-center'>
					<?= $error ?>
				</div>
			</div>
		<?php endif; ?>
	</div>
	<br/>
	<div class='container'>
		<?php Layout\printFooter(); ?>
	</div>
</body>

<?php Bonus\includeDR(); ?>

</html>
