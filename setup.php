<?php

try {
    $env = parse_ini_file('.env');
	$connection_string = "mysql:host={$env['MARIADB_HOST']};port={$env['MARIADB_PORT']};dbname={$env['MARIADB_DATABASE']}";
    $pdo = new PDO(
		$connection_string,
        $env['MARIADB_USER'],
        $env['MARIADB_ROOT_PASSWORD']
    );
    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    // Create tables if they don't exist
    $pdo->exec("CREATE TABLE IF NOT EXISTS users (
        id INT AUTO_INCREMENT PRIMARY KEY,
        email TEXT NOT NULL UNIQUE,
        verified INTEGER,
        comment_notify INTEGER,
        username TEXT NOT NULL,
        password TEXT NOT NULL,
        theme TEXT NOT NULL
    );");

    // waiting for == if not null, in user's waiting list
    $pdo->exec("CREATE TABLE IF NOT EXISTS pictures (
        id INT AUTO_INCREMENT PRIMARY KEY,
        waiting_for INT,
        data LONGBLOB,
        FOREIGN KEY(waiting_for) REFERENCES users(id)
    );");

    $pdo->exec("CREATE TABLE IF NOT EXISTS posts (
        id INT AUTO_INCREMENT PRIMARY KEY,
        picture INT NOT NULL,
        owner INT NOT NULL,
        date TIMESTAMP NOT NULL,
        FOREIGN KEY(owner) REFERENCES users(id),
        FOREIGN KEY(picture) REFERENCES pictures(id)
    );");

    $pdo->exec("CREATE TABLE IF NOT EXISTS post_comments (
        id INT AUTO_INCREMENT PRIMARY KEY,
        content TEXT NOT NULL,
        user_id INT NOT NULL,
        post_id INT NOT NULL,
        date TIMESTAMP NOT NULL,
        FOREIGN KEY(user_id) REFERENCES users(id),
        FOREIGN KEY(post_id) REFERENCES posts(id)
    );");

    $pdo->exec("CREATE TABLE IF NOT EXISTS post_likes (
        user_id INT NOT NULL,
        post_id INT NOT NULL,
        FOREIGN KEY(user_id) REFERENCES users(id),
        FOREIGN KEY(post_id) REFERENCES posts(id)
    );");

    $pdo->exec("CREATE TABLE IF NOT EXISTS email_types (
        id INT AUTO_INCREMENT PRIMARY KEY,
        name TEXT NOT NULL UNIQUE
    );");

    $pdo->exec("CREATE TABLE IF NOT EXISTS sent_emails (
        uid TEXT NOT NULL UNIQUE,
        time TIMESTAMP NOT NULL,
        user INT NOT NULL,
        type INT NOT NULL,
        FOREIGN KEY(user) REFERENCES users(id),
        FOREIGN KEY(type) REFERENCES email_types(id)
    );");

    $pdo->exec("INSERT INTO
            email_types (name)
        VALUES
            ('verification'), ('password_reset')
        ON DUPLICATE KEY UPDATE name = name;");

    $pdo->exec('CREATE TABLE IF NOT EXISTS cookie_tokens (
        user INT NOT NULL UNIQUE,
        token TEXT NOT NULL UNIQUE,
        FOREIGN KEY(user) REFERENCES users(id)
    );');

    $pdo->exec("CREATE TABLE IF NOT EXISTS sessions (
        id VARCHAR(255) NOT NULL PRIMARY KEY,
        data TEXT,
        last_accessed TIMESTAMP NOT NULL
    );");

    $pdo = null; // Close the PDO connection
} catch (PDOException $e) {
    error_log($e->getMessage());
    exit(1);
}
