FROM php:8.2-fpm-bookworm

# Install sendmail and ssmtp
RUN apt-get update && apt-get install -y \
	nginx \
	sassc \
	ssmtp

# Install GD and JPEG support
RUN apt-get install -y zlib1g-dev libpng-dev libjpeg-dev \
	&& docker-php-ext-configure gd --with-jpeg \
	&& docker-php-ext-install -j$(nproc) gd

# Install MySQL PDO and dependencies
RUN apt-get update \
    && apt-get install -y libzip-dev \
    && docker-php-ext-configure pdo_mysql \
    && docker-php-ext-install -j$(nproc) pdo_mysql

# generating TLS for email
RUN apt-get install -y openssl \
    && openssl genrsa -out private.key 2048 \
    && openssl req -new -key private.key -out csr.csr -subj "/C=BE/ST=Brussels/L=Brussels/O=campus19/CN=camagru.mlefevre.xyz" \
    && openssl x509 -req -days 365 -in csr.csr -signkey private.key -out certificate.crt \
    && cp certificate.crt /etc/ssl/certs/ca-certificates.crt

# Copy files to img
COPY . /var/www/

# Move files that need to
RUN mv /var/www/nginx.conf /etc/nginx/nginx.conf \
&& mv /var/www/php.ini /usr/local/etc/php/php.ini \
&& mv /var/www/ssmtp.conf /etc/ssmtp/ssmtp.conf \
&& mkdir -p /var/www/public/styles/bootstrap

# Configure the PHP mail function
RUN echo "[mail function]\nsendmail_path = \"/usr/sbin/ssmtp -t\"" > /usr/local/etc/php/conf.d/mail.ini
RUN chmod 640 /etc/ssmtp/ssmtp.conf && \
	chown root:mail /etc/ssmtp/ssmtp.conf && \
	usermod -aG mail www-data

# fill smtp password field
RUN cat /var/www/.env | grep AuthPass >> /etc/ssmtp/ssmtp.conf

# deleting debug dir (for security)
RUN rm -rf /var/www/public/debug

# Generating css
RUN mkdir -p /var/www/public/styles/bootstrap && sassc --style compressed \
	/var/www/src/sass/bootstrap/bootstrap.scss \
	/var/www/public/styles/bootstrap/bootstrap.css
RUN sassc --style compressed \
	/var/www/src/sass/main.scss \
	/var/www/public/styles/main.css

# Set up permissions
RUN chown -R www-data:www-data /var/www

# Expose port 80 for HTTP
EXPOSE 80

CMD php /var/www/setup.php && php-fpm -D && nginx -g 'daemon off;'
