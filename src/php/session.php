<?php

require_once($_SERVER['DOCUMENT_ROOT'] . '/../src/php/db.php');

class MariaDBSessionHandler implements SessionHandlerInterface
{
	private $db;

	public function open(string $savePath, string $sessionName): bool
	{
		// savePath is for file-based sessions. Not in use here
		// session name is used for the cookie. Not for us.
		try {
			$this->db = connectDb(); // Assuming connectDb() connects to MariaDB
		} catch (Exception $e) {
			error_log($e->getMessage());
			return false;
		}
		return true;
	}

	public function close(): bool
	{
		$this->db = null; // Closing PDO connection by setting it to null
		return true;
	}

	public function read(string $sessionId): string
	{
		$stmt = $this->db->prepare("SELECT data FROM sessions WHERE id = :sessionId");
		$stmt->bindValue(':sessionId', $sessionId);
		$stmt->execute();
		$row = $stmt->fetch(PDO::FETCH_ASSOC);
		return $row ? $row['data'] : '';
	}

	public function write(string $sessionId, string $data): bool
	{
		$stmt = $this->db->prepare(
			"INSERT INTO sessions (id, data, last_accessed)
            VALUES (:sessionId, :data, NOW())
            ON DUPLICATE KEY UPDATE data = :data, last_accessed = NOW()"
		);
		$stmt->bindValue(':sessionId', $sessionId);
		$stmt->bindValue(':data', $data);
		return $stmt->execute();
	}

	public function destroy(string $sessionId): bool
	{
		$stmt = $this->db->prepare("DELETE FROM sessions WHERE id = :sessionId");
		$stmt->bindValue(':sessionId', $sessionId);
		return $stmt->execute();
	}

	public function gc(int $maxLifetime): int | false
	{
		$stmt = $this->db->prepare(
			"DELETE FROM sessions WHERE last_accessed < NOW() - INTERVAL {$maxLifetime} SECOND"
		);
		$stmt->execute();
		return $stmt->rowCount();
	}
}

session_set_save_handler(new MariaDBSessionHandler(), true);
session_start();
