<?php
/*
 * Include this file in the head of every main html page
*/

// so that the metadata adapts in case of url change
function getBaseUrl()
{
	$protocol = (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off') ? "https" : "http";
	$host = $_SERVER['HTTP_HOST'];
	return "$protocol://$host";
}
$base_url = getBaseUrl();
?>

<!-- Favicon -->
<link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png">
<link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
<link rel="manifest" href="/site.webmanifest">

<!-- Open Graph Metadata -->
<meta property="og:title" content="Camagru">
<meta property="og:description" content="Come and share some pictures on Camagru!">
<meta property="og:image" content="<?= $base_url ?>/og-preview.png">
<meta property="og:url" content="<?= $base_url ?>">
<meta property="og:type" content="website">
