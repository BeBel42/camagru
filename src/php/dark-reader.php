<?php

namespace Bonus;

// This includes the dark reader js (BONUS)

require_once($_SERVER['DOCUMENT_ROOT'] . '/../src/php/session.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/../src/php/db.php');


function includeDR()
{
?>
	<script src="https://cdn.jsdelivr.net/npm/darkreader@4.9.66/darkreader.min.js" />
	</script>
	<script>
		function getFromData() {
			// to stop having CORS error
			DarkReader.setFetchMethod(window.fetch);

			<?php
			$dark_enabled = (
				!empty($_SESSION['theme']) && $_SESSION['theme'] === 'dark'
			);
			if (empty($_SESSION['theme']) && !empty($_SESSION['logged_user'])) {
				try {
					$db = connectDb();
					$user = getUserById($db, $_SESSION['logged_user']);
					$dark_enabled = ($user['theme'] !== 'white');
					$_SESSION['theme'] = $user['theme'];
				} catch (\Exception $e) {
					error_log($e->getMessage());
				}
			}
			?>
			<?php if (!$dark_enabled) : ?>
				DarkReader.disable();
			<?php else : ?>
				DarkReader.enable({
					brightness: 100,
					contrast: 90,
					sepia: 20
				});
			<?php endif; ?>
		}
		getFromData();
	</script>
<?php
}

function DRButton()
{
?>
	<btn title='Toggle theme' style='width: 10em' id='dr-btn' onclick="toggleDR()" class='btn btn-primary'>Light</btn>
<?php
}
