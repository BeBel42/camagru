<?php

/*
  all functions using the db will be put here
*/

function connectDb(): PDO
{
	$projectRoot = $_SERVER['DOCUMENT_ROOT'];
	$envPath = $projectRoot . '/../.env';

	try {
		$env = parse_ini_file($envPath);
		$pdo = new PDO(
			"mysql:host={$env['MARIADB_HOST']};port={$env['MARIADB_PORT']};dbname={$env['MARIADB_DATABASE']}",
			$env['MARIADB_USER'],
			$env['MARIADB_ROOT_PASSWORD']
		);
		$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	} catch (PDOException $e) {
		throw new Exception("Could not connect to database. " . $e->getMessage());
	}

	return $pdo;
}

// signup.php - user.php
function isEmailNew(PDO $pdo, string $email): bool
{
	try {
		$stmt = $pdo->prepare("
            SELECT email FROM users
            WHERE email = :email
        ");
		if (!$stmt) {
			throw new Exception("Internal server error: " . implode(" ", $pdo->errorInfo()));
		}

		$stmt->bindParam(':email', $email, PDO::PARAM_STR);
		if (!$stmt->execute()) {
			return false;
		}

		$res = $stmt->fetch(PDO::FETCH_ASSOC);

		return ($res === false);
	} catch (Exception $e) {
		error_log($e->getMessage());
		throw $e;
	} finally {
		if ($stmt !== null) {
			$stmt->closeCursor();
		}
	}
}

// signup.php
function createUser(PDO $pdo, string $username, string $password, string $email): int
{
	try {
		$stmt = $pdo->prepare("
            INSERT INTO users
            (email, username, password, comment_notify, theme)
            VALUES
            (:email, :username, :password, 1, 'white');
        ");
		if (!$stmt) {
			throw new Exception("Internal server error: " . implode(" ", $pdo->errorInfo()));
		}

		$hashedPassword = password_hash($password, PASSWORD_DEFAULT);

		$stmt->bindParam(':email', $email, PDO::PARAM_STR);
		$stmt->bindParam(':username', $username, PDO::PARAM_STR);
		$stmt->bindParam(':password', $hashedPassword, PDO::PARAM_STR);

		if (!$stmt->execute()) {
			throw new Exception("Internal server error: " . implode(" ", $stmt->errorInfo()));
		}

		$id = $pdo->lastInsertId();

		return $id;
	} catch (Exception $e) {
		error_log($e->getMessage());
		throw $e;
	} finally {
		if ($stmt !== null) {
			$stmt->closeCursor();
		}
	}
}

// confirm-email.php
function setEmailId(PDO $pdo, string $email, string $type): string
{
	try {
		$uid = bin2hex(random_bytes(20));
		$stmt = $pdo->prepare("
            INSERT INTO sent_emails (uid, time, user, type)
            VALUES (:uid, NOW(), (SELECT id FROM users WHERE email = :email),
				(SELECT id FROM email_types WHERE name = :type));
        ");
		if (!$stmt) {
			throw new Exception("Internal server error: " . implode(" ", $pdo->errorInfo()));
		}

		$stmt->bindParam(':uid', $uid, PDO::PARAM_STR);
		$stmt->bindParam(':email', $email, PDO::PARAM_STR);
		$stmt->bindParam(':type', $type, PDO::PARAM_STR);

		if (!$stmt->execute()) {
			throw new Exception("Internal server error: " . implode(" ", $stmt->errorInfo()));
		}

		return $uid;
	} catch (Exception $e) {
		error_log($e->getMessage());
		throw $e;
	} finally {
		if ($stmt !== null) {
			$stmt->closeCursor();
		}
	}
}

// confirm-email.php
function cleanUids(PDO $pdo): void
{
	try {
		$sql = "
            DELETE FROM sent_emails
            WHERE NOW() - INTERVAL 5 MINUTE > time;
        ";

		$result = $pdo->exec($sql);

		if ($result === false) {
			throw new Exception("Internal server error: " . implode(" ", $pdo->errorInfo()));
		}
	} catch (Exception $e) {
		error_log($e->getMessage());
		throw $e;
	}
}

// confirm-email.php
function isUidValid(PDO $pdo, string $uid, string $type): bool
{
	try {
		$stmt = $pdo->prepare("
            SELECT uid FROM sent_emails
            WHERE uid = :uid
            AND type = (SELECT id FROM email_types WHERE name = :type)
        ");

		if (!$stmt) {
			throw new Exception("Internal server error: " . implode(" ", $pdo->errorInfo()));
		}

		$stmt->bindParam(':uid', $uid, PDO::PARAM_STR);
		$stmt->bindParam(':type', $type, PDO::PARAM_STR);

		if (!$stmt->execute()) {
			throw new Exception("Internal server error: " . implode(" ", $stmt->errorInfo()));
		}

		$res = $stmt->fetch(PDO::FETCH_ASSOC);

		return ($res !== false);
	} catch (Exception $e) {
		error_log($e->getMessage());
		throw $e;
	} finally {
		if ($stmt !== null) {
			$stmt->closeCursor();
		}
	}
}

// confirm-email.php
function verifyEmail(PDO $pdo, string $uid): void
{
	try {
		$stmt = $pdo->prepare("
            UPDATE users
            SET verified = 1
            WHERE id = (
                SELECT user
                FROM sent_emails
                WHERE uid = :uid
            );
        ");

		if (!$stmt) {
			throw new Exception("Internal server error: " . implode(" ", $pdo->errorInfo()));
		}

		$stmt->bindParam(':uid', $uid, PDO::PARAM_STR);

		if (!$stmt->execute()) {
			throw new Exception("Internal server error: " . implode(" ", $stmt->errorInfo()));
		}
	} catch (Exception $e) {
		error_log($e->getMessage());
		throw $e;
	} finally {
		if ($stmt !== null) {
			$stmt->closeCursor();
		}
	}
}

function unverifyEmail(PDO $pdo, int $user_id): void
{
	try {
		$stmt = $pdo->prepare("
            UPDATE users
            SET verified = NULL
            WHERE id = :user_id
        ");

		if (!$stmt) {
			throw new Exception("Internal server error: " . implode(" ", $pdo->errorInfo()));
		}

		$stmt->bindParam(':user_id', $user_id, PDO::PARAM_INT);

		if (!$stmt->execute()) {
			throw new Exception("Internal server error: " . implode(" ", $stmt->errorInfo()));
		}
	} catch (Exception $e) {
		error_log($e->getMessage());
		throw $e;
	} finally {
		if ($stmt !== null) {
			$stmt->closeCursor();
		}
	}
}

// confirm-email.php
function alreadySent(PDO $pdo, string $email, string $type): bool
{
	try {
		$stmt = $pdo->prepare("
            SELECT uid FROM sent_emails
            WHERE user = (SELECT id FROM users WHERE email = :email)
            AND type = (SELECT id FROM email_types WHERE name = :type)
        ");

		if (!$stmt) {
			throw new Exception("Internal server error: " . implode(" ", $pdo->errorInfo()));
		}

		$stmt->bindParam(':email', $email, PDO::PARAM_STR);
		$stmt->bindParam(':type', $type, PDO::PARAM_STR);

		if (!$stmt->execute()) {
			throw new Exception("Internal server error: " . implode(" ", $stmt->errorInfo()));
		}

		$res = $stmt->fetch(PDO::FETCH_ASSOC);

		return ($res !== false);
	} catch (Exception $e) {
		error_log($e->getMessage());
		throw $e;
	} finally {
		if ($stmt !== null) {
			$stmt->closeCursor();
		}
	}
}

// login.php
function logIn(PDO $pdo, string $email, string $password): int | bool
{
	try {
		$stmt = $pdo->prepare("
            SELECT id, password FROM users
            WHERE email = :email
        ");

		if (!$stmt) {
			throw new Exception("Internal server error: " . implode(" ", $pdo->errorInfo()));
		}

		$stmt->bindParam(':email', $email, PDO::PARAM_STR);

		if (!$stmt->execute()) {
			throw new Exception("Internal server error: " . implode(" ", $stmt->errorInfo()));
		}

		$res = $stmt->fetch(PDO::FETCH_ASSOC);

		// no user with email
		if ($res === false) {
			return false;
		}

		// incorrect password
		if (password_verify($password, $res['password']) === false) {
			return false;
		}

		return (int)$res['id'];
	} catch (Exception $e) {
		error_log($e->getMessage());
		throw $e;
	} finally {
		if ($stmt !== null) {
			$stmt->closeCursor();
		}
	}
}

// login.php
function isVerified(PDO $pdo, string $email): bool
{
	try {
		$stmt = $pdo->prepare("
            SELECT verified FROM users
            WHERE email = :email
        ");

		if (!$stmt) {
			throw new Exception("Internal server error: " . implode(" ", $pdo->errorInfo()));
		}

		$stmt->bindParam(':email', $email, PDO::PARAM_STR);

		if (!$stmt->execute()) {
			throw new Exception("Internal server error: " . implode(" ", $stmt->errorInfo()));
		}

		$res = $stmt->fetch(PDO::FETCH_ASSOC);

		// no user with email
		if ($res === false) {
			return false;
		}

		// not verified
		return ($res['verified'] === 1);
	} catch (Exception $e) {
		error_log($e->getMessage());
		throw $e;
	} finally {
		if ($stmt !== null) {
			$stmt->closeCursor();
		}
	}
}

// login.php - password-reset.php
function getUserByEmail(PDO $pdo, string $email): array
{
	try {
		$stmt = $pdo->prepare("
            SELECT * FROM users
            WHERE email = :email
        ");

		if (!$stmt) {
			throw new Exception("Internal server error: " . implode(" ", $pdo->errorInfo()));
		}

		$stmt->bindParam(':email', $email, PDO::PARAM_STR);

		if (!$stmt->execute()) {
			throw new Exception("Internal server error: " . implode(" ", $stmt->errorInfo()));
		}

		// no user with email
		$res = $stmt->fetch(PDO::FETCH_ASSOC);

		if ($res === false) {
			throw new Exception('No user with email exists');
		}

		return $res;
	} catch (Exception $e) {
		error_log($e->getMessage());
		throw $e;
	} finally {
		if ($stmt !== null) {
			$stmt->closeCursor();
		}
	}
}

// password-reset.php
function updatePassword(PDO $pdo, string $uid): string
{
	try {
		$stmt = $pdo->prepare("
            UPDATE users
            SET password = :pwd
            WHERE id = (
                SELECT user
                FROM sent_emails
                WHERE uid = :uid
            );
        ");

		if (!$stmt)
			throw new Exception("Internal server error: " . implode(" ", $pdo->errorInfo()));

		$newPwd = bin2hex(random_bytes(6));
		$hashedPassword = password_hash($newPwd, PASSWORD_DEFAULT);

		$stmt->bindParam(':pwd', $hashedPassword, PDO::PARAM_STR);
		$stmt->bindParam(':uid', $uid, PDO::PARAM_STR);

		if (!$stmt->execute())
			throw new Exception("Internal server error: " . implode(" ", $stmt->errorInfo()));

		return $newPwd;
	} catch (Exception $e) {
		error_log($e->getMessage());
		throw $e;
	} finally {
		if ($stmt !== null)
			$stmt->closeCursor();
	}
}

// many files
function getUserById(PDO $pdo, int $id): array
{
	try {
		$stmt = $pdo->prepare("
            SELECT * FROM users
            WHERE id = :id
        ");

		if (!$stmt)
			throw new Exception("Internal server error: " . implode(" ", $pdo->errorInfo()));

		$stmt->bindParam(':id', $id, PDO::PARAM_INT);

		if (!$stmt->execute())
			throw new Exception("Internal server error: " . implode(" ", $stmt->errorInfo()));

		// no user with id
		$res = $stmt->fetch(PDO::FETCH_ASSOC);

		if ($res === false)
			throw new Exception('No user with id exists');

		return $res;
	} catch (Exception $e) {
		error_log($e->getMessage());
		throw $e;
	} finally {
		if ($stmt !== null) {
			$stmt->closeCursor();
		}
	}
}

// user.php
function updateUser(PDO $pdo, array $user): void
{
	try {
		// deleting sent emails for current email
		$stmt = $pdo->prepare("
            DELETE from sent_emails
            WHERE user = :user_id;
        ");
		if (!$stmt)
			throw new Exception("Internal server error: " . implode(" ", $pdo->errorInfo()));
		$stmt->bindParam(':user_id', $user['id'], PDO::PARAM_INT);
		if (!$stmt->execute())
			throw new Exception("Internal server error: " . implode(" ", $stmt->errorInfo()));

		// updating user
		$stmt = $pdo->prepare("
            UPDATE users
            SET email = :email, password = :password, username = :username
            WHERE id = :id
        ");
		if (!$stmt)
			throw new Exception("Internal server error: " . implode(" ", $pdo->errorInfo()));
		$stmt->bindParam(':id', $user['id'], PDO::PARAM_INT);
		$stmt->bindParam(':email', $user['email'], PDO::PARAM_STR);
		$stmt->bindParam(':password', $user['password'], PDO::PARAM_STR);
		$stmt->bindParam(':username', $user['username'], PDO::PARAM_STR);
		if (!$stmt->execute())
			throw new Exception("Internal server error: " . implode(" ", $stmt->errorInfo()));
	} catch (Exception $e) {
		error_log($e->getMessage());
		throw $e;
	} finally {
		if ($stmt !== null)
			$stmt->closeCursor();
	}
}

// login.php
function deleteCookieToken(PDO $pdo, int $userId): void
{
	try {
		$stmt = $pdo->prepare('
            DELETE FROM cookie_tokens
            WHERE user = :id
        ');

		if (!$stmt)
			throw new Exception("Internal server error: " . implode(" ", $pdo->errorInfo()));

		$stmt->bindParam(':id', $userId, PDO::PARAM_INT);

		if (!$stmt->execute())
			throw new Exception("Internal server error: " . implode(" ", $stmt->errorInfo()));
	} catch (Exception $e) {
		error_log($e->getMessage());
		throw $e;
	} finally {
		if ($stmt !== null) {
			$stmt->closeCursor();
		}
	}
}

// login.php
function setCookieToken(PDO $pdo, int $userId): string
{
	try {
		$stmt = $pdo->prepare('
			INSERT INTO cookie_tokens (user, token) VALUES (:id, :token)
			ON DUPLICATE KEY UPDATE token = :token;
        ');

		if (!$stmt)
			throw new Exception("Internal server error: " . implode(" ", $pdo->errorInfo()));

		$stmt->bindParam(':id', $userId, PDO::PARAM_INT);
		$token = bin2hex(random_bytes(20));
		$stmt->bindParam(':token', $token, PDO::PARAM_STR);

		if (!$stmt->execute())
			throw new Exception("Internal server error: " . implode(" ", $stmt->errorInfo()));

		return $token;
	} catch (Exception $e) {
		error_log($e->getMessage());
		throw $e;
	} finally {
		if ($stmt !== null) {
			$stmt->closeCursor();
		}
	}
}

// login.php
function getUserFromToken(PDO $pdo, string $token): array
{
	try {
		$stmt = $pdo->prepare('
            SELECT * FROM users
            WHERE id = (
                SELECT user FROM cookie_tokens WHERE token = :token
            )
        ');

		if (!$stmt) {
			throw new Exception("Internal server error: " . implode(" ", $pdo->errorInfo()));
		}

		$stmt->bindParam(':token', $token, PDO::PARAM_STR);

		if (!$stmt->execute()) {
			throw new Exception("Internal server error: " . implode(" ", $stmt->errorInfo()));
		}

		// no user with token
		$res = $stmt->fetch(PDO::FETCH_ASSOC);

		if ($res === false) {
			throw new Exception('No user with token exists');
		}

		return $res;
	} catch (Exception $e) {
		error_log($e->getMessage());
		throw $e;
	} finally {
		if ($stmt !== null) {
			$stmt->closeCursor();
		}
	}
}

// submit.php
function createWaitingPicture(PDO $pdo, int $user_id, string $img_b): void
{
	try {
		$stmt = $pdo->prepare("
            INSERT INTO pictures (data, waiting_for)
            VALUES (:img_b, :user_id)
        ");

		if (!$stmt) {
			throw new Exception("Internal server error: " . implode(" ", $pdo->errorInfo()));
		}

		$stmt->bindParam(':img_b', $img_b, PDO::PARAM_LOB);
		$stmt->bindParam(':user_id', $user_id, PDO::PARAM_INT);

		if (!$stmt->execute()) {
			throw new Exception("Internal server error: " . implode(" ", $stmt->errorInfo()));
		}
	} catch (Exception $e) {
		error_log($e->getMessage());
		throw $e;
	} finally {
		if ($stmt !== null) {
			$stmt->closeCursor();
		}
	}
}

// get-waiting-pic.php
function getWaitingPicture(PDO $pdo, int $user_id, int $img_id): string
{
	try {
		$stmt = $pdo->prepare("
            SELECT * FROM pictures
            WHERE waiting_for = :user_id AND id = :img_id
        ");

		if (!$stmt)
			throw new Exception("Internal server error: " . implode(" ", $pdo->errorInfo()));

		$stmt->bindParam(':user_id', $user_id, PDO::PARAM_INT);
		$stmt->bindParam(':img_id', $img_id, PDO::PARAM_INT);

		if (!$stmt->execute())
			throw new Exception("Internal server error: " . implode(" ", $stmt->errorInfo()));

		$res = $stmt->fetch(PDO::FETCH_ASSOC);

		if ($res === false)
			throw new Exception("Internal server error: " . implode(" ", $stmt->errorInfo()));

		$data = $res['data'];
		if ($data === false)
			throw new Exception("Internal server error: could not read stream content");
		return $data;
	} catch (Exception $e) {
		error_log($e->getMessage());
		throw $e;
	} finally {
		if ($stmt !== null)
			$stmt->closeCursor();
	}
}

// get-post-pic.php
function getPostPicture(PDO $pdo, int $img_id): string
{
	try {
		$stmt = $pdo->prepare("
            SELECT * FROM pictures
            WHERE waiting_for IS NULL AND id = :img_id
        ");

		if (!$stmt) {
			throw new Exception("Internal server error: " . implode(" ", $pdo->errorInfo()));
		}

		$stmt->bindParam(':img_id', $img_id, PDO::PARAM_INT);

		if (!$stmt->execute()) {
			throw new Exception("Internal server error: " . implode(" ", $stmt->errorInfo()));
		}

		$res = $stmt->fetch(PDO::FETCH_ASSOC);

		if ($res === false) {
			throw new Exception("Internal server error: " . implode(" ", $stmt->errorInfo()));
		}

		$data = $res['data'];
		if ($data === false)
			throw new Exception("Internal server error: could not read stream content");
		return $data;
	} catch (Exception $e) {
		error_log($e->getMessage());
		throw $e;
	} finally {
		if ($stmt !== null) {
			$stmt->closeCursor();
		}
	}
}

// delete-waiting-pic.php
function deleteWaitingPicture(PDO $pdo, int $user_id, int $img_id): void
{
	try {
		$stmt = $pdo->prepare("
            DELETE FROM pictures
            WHERE waiting_for = :user_id AND id = :img_id
        ");

		if (!$stmt) {
			throw new Exception("Internal server error: " . implode(" ", $pdo->errorInfo()));
		}

		$stmt->bindParam(':user_id', $user_id, PDO::PARAM_INT);
		$stmt->bindParam(':img_id', $img_id, PDO::PARAM_INT);

		if (!$stmt->execute()) {
			throw new Exception("Internal server error: " . implode(" ", $stmt->errorInfo()));
		}
	} catch (Exception $e) {
		error_log($e->getMessage());
		throw $e;
	} finally {
		if ($stmt !== null) {
			$stmt->closeCursor();
		}
	}
}

// get-waiting-pics.php
function getWaitingPictureList(PDO $pdo, int $user_id): array
{
	try {
		$stmt = $pdo->prepare("
            SELECT id FROM pictures
            WHERE waiting_for = :user_id
        ");

		if (!$stmt) {
			throw new Exception("Internal server error: " . implode(" ", $pdo->errorInfo()));
		}

		$stmt->bindParam(':user_id', $user_id, PDO::PARAM_INT);

		if (!$stmt->execute()) {
			throw new Exception("Internal server error: " . implode(" ", $stmt->errorInfo()));
		}

		$r = [];

		while ($res = $stmt->fetch(PDO::FETCH_ASSOC)) {
			array_push($r, $res['id']);
		}

		return $r;
	} catch (Exception $e) {
		error_log($e->getMessage());
		throw $e;
	} finally {
		if ($stmt !== null) {
			$stmt->closeCursor();
		}
	}
}

// publish.php
function submit_post(PDO $pdo, int $user_id, string $img_id): void
{
	$pdo->beginTransaction(); // Start a transaction
	try {
		// Insert the post into the 'posts' table
		$stmtInsertPost = $pdo->prepare("
            INSERT INTO posts (picture, owner, date)
            SELECT id, waiting_for, NOW() FROM pictures
            WHERE id = :img_id AND waiting_for = :user_id
        ");

		if (!$stmtInsertPost) {
			throw new Exception("Internal server error: " . implode(" ", $pdo->errorInfo()));
		}

		$stmtInsertPost->bindParam(':img_id', $img_id, PDO::PARAM_INT);
		$stmtInsertPost->bindParam(':user_id', $user_id, PDO::PARAM_INT);

		if (!$stmtInsertPost->execute()) {
			throw new Exception("Internal server error: " . implode(" ", $stmtInsertPost->errorInfo()));
		}

		// Update the 'pictures' table to clear 'waiting_for'
		$stmtUpdatePicture = $pdo->prepare("
            UPDATE pictures
            SET waiting_for = NULL
            WHERE waiting_for = :user_id AND id = :img_id
        ");

		if (!$stmtUpdatePicture) {
			throw new Exception("Internal server error: " . implode(" ", $pdo->errorInfo()));
		}

		$stmtUpdatePicture->bindParam(':img_id', $img_id, PDO::PARAM_INT);
		$stmtUpdatePicture->bindParam(':user_id', $user_id, PDO::PARAM_INT);

		if (!$stmtUpdatePicture->execute()) {
			throw new Exception("Internal server error: " . implode(" ", $stmtUpdatePicture->errorInfo()));
		}

		// Commit the transaction
		$pdo->commit();
	} catch (Exception $e) {
		// Rollback the transaction on error
		$pdo->rollBack();
		error_log($e->getMessage());
		throw $e;
	}
}

// index.php
function get_post_before(PDO $pdo, int | null $post_before, int | null $user_id): array
{
	try {
		$query = "
            SELECT
                users.id AS userid,
                posts.id AS postid,
                posts.picture,
                users.username,
                users.email,
                posts.date
            FROM posts
            INNER JOIN users ON posts.owner = users.id";

		if ($user_id !== null) {
			$query .= " WHERE posts.owner = :user_id";
		}

		if ($post_before !== null) {
			if ($user_id !== null) {
				$query .= " AND";
			} else {
				$query .= " WHERE";
			}
			$query .= " posts.date < (
                SELECT date FROM posts WHERE id = :post_before
            )";
		}

		$query .= " ORDER BY posts.date DESC LIMIT 1";

		$stmt = $pdo->prepare($query);

		if (!$stmt) {
			throw new Exception("Internal server error: " . implode(" ", $pdo->errorInfo()));
		}

		if ($user_id !== null) {
			$stmt->bindParam(':user_id', $user_id, PDO::PARAM_INT);
		}

		if ($post_before !== null) {
			$stmt->bindParam(':post_before', $post_before, PDO::PARAM_INT);
		}

		if (!$stmt->execute()) {
			throw new Exception("Internal server error: " . implode(" ", $stmt->errorInfo()));
		}

		$r = $stmt->fetch(PDO::FETCH_ASSOC);

		return $r ?: [];
	} catch (Exception $e) {
		error_log($e->getMessage());
		throw $e;
	} finally {
		if ($stmt !== null) {
			$stmt->closeCursor();
		}
	}
}

// toggle-like.php
function toggle_like(PDO $pdo, int $user_id, int $post_id): void
{
	$pdo->beginTransaction(); // Start a transaction
	try {
		// Check if the user has already liked the post
		$query = "SELECT COUNT(*) FROM post_likes
            WHERE user_id = :user_id AND post_id = :post_id";

		$stmtCheckLike = $pdo->prepare($query);

		if (!$stmtCheckLike) {
			throw new Exception("Internal server error: " . implode(" ", $pdo->errorInfo()));
		}

		$stmtCheckLike->bindParam(':user_id', $user_id, PDO::PARAM_INT);
		$stmtCheckLike->bindParam(':post_id', $post_id, PDO::PARAM_INT);

		if (!$stmtCheckLike->execute()) {
			throw new Exception("Internal server error: " . implode(" ", $stmtCheckLike->errorInfo()));
		}

		$has_liked = ($stmtCheckLike->fetchColumn() !== 0);

		$stmtCheckLike->closeCursor();

		if ($has_liked) {
			// User has already liked the post, so unlike it
			$query = "DELETE FROM post_likes
                WHERE user_id = :user_id AND post_id = :post_id";
		} else {
			// User hasn't liked the post, so like it
			$query = "INSERT INTO post_likes (user_id, post_id)
                VALUES (:user_id, :post_id)";
		}

		$stmtToggleLike = $pdo->prepare($query);

		if (!$stmtToggleLike) {
			throw new Exception("Internal server error: " . implode(" ", $pdo->errorInfo()));
		}

		$stmtToggleLike->bindParam(':user_id', $user_id, PDO::PARAM_INT);
		$stmtToggleLike->bindParam(':post_id', $post_id, PDO::PARAM_INT);

		if (!$stmtToggleLike->execute()) {
			throw new Exception("Internal server error: " . implode(" ", $stmtToggleLike->errorInfo()));
		}

		// Commit the transaction
		$pdo->commit();
	} catch (Exception $e) {
		// Rollback the transaction on error
		$pdo->rollBack();
		error_log($e->getMessage());
		throw $e;
	}
}

// get-like.php
function get_like_status(PDO $pdo, int $user_id, int $post_id): array
{
	try {
		$has_liked = false;
		// if logged in
		if ($user_id !== -1) {
			// Check if the user has liked the post
			$query1 = "SELECT COUNT(*) FROM post_likes
            WHERE user_id = :user_id AND post_id = :post_id";

			$stmtCheckLike = $pdo->prepare($query1);

			if (!$stmtCheckLike) {
				throw new Exception("Internal server error: " . implode(" ", $pdo->errorInfo()));
			}

			$stmtCheckLike->bindParam(':user_id', $user_id, PDO::PARAM_INT);
			$stmtCheckLike->bindParam(':post_id', $post_id, PDO::PARAM_INT);

			if (!$stmtCheckLike->execute()) {
				throw new Exception("Internal server error: " . implode(" ", $stmtCheckLike->errorInfo()));
			}

			$has_liked = ($stmtCheckLike->fetchColumn() !== 0);

			$stmtCheckLike->closeCursor();
		}

		// Get the total number of likes for the post
		$query2 = "SELECT COUNT(*) FROM post_likes WHERE post_id = :post_id";

		$stmtGetLikeCount = $pdo->prepare($query2);

		if (!$stmtGetLikeCount) {
			throw new Exception("Internal server error: " . implode(" ", $pdo->errorInfo()));
		}

		$stmtGetLikeCount->bindParam(':post_id', $post_id, PDO::PARAM_INT);

		if (!$stmtGetLikeCount->execute()) {
			throw new Exception("Internal server error: " . implode(" ", $stmtGetLikeCount->errorInfo()));
		}

		$count = (int)$stmtGetLikeCount->fetchColumn();

		return ['likes' => $count, 'isLiked' => $has_liked];
	} catch (Exception $e) {
		error_log($e->getMessage());
		throw $e;
	}
}

// submit-comment.php
function submit_comment(PDO $pdo, int $user_id, int $post_id, string $post_text): void
{
	try {
		$query = "INSERT INTO post_comments (content, user_id, post_id, date)
                  VALUES (:post_text, :user_id, :post_id, NOW())";

		$stmt = $pdo->prepare($query);

		if (!$stmt) {
			throw new Exception("Internal server error: " . implode(" ", $pdo->errorInfo()));
		}

		$stmt->bindParam(':user_id', $user_id, PDO::PARAM_INT);
		$stmt->bindParam(':post_id', $post_id, PDO::PARAM_INT);
		$stmt->bindParam(':post_text', $post_text, PDO::PARAM_STR);

		if (!$stmt->execute()) {
			throw new Exception("Internal server error: " . implode(" ", $stmt->errorInfo()));
		}
	} catch (Exception $e) {
		error_log($e->getMessage());
		throw $e;
	}
}

// get-oomments.php
function get_comments(PDO $pdo, int $post_id): array
{
	try {
		$query = "SELECT
            post_comments.id,
            post_comments.user_id,
            post_comments.post_id,
            post_comments.date,
            post_comments.content,
            users.username
        FROM post_comments
        INNER JOIN users ON post_comments.user_id = users.id
        WHERE post_comments.post_id = :post_id
        ORDER BY post_comments.date ASC";

		$stmt = $pdo->prepare($query);

		if (!$stmt) {
			throw new Exception("Internal server error: " . implode(" ", $pdo->errorInfo()));
		}

		$stmt->bindParam(':post_id', $post_id, PDO::PARAM_INT);

		if (!$stmt->execute()) {
			throw new Exception("Internal server error: " . implode(" ", $stmt->errorInfo()));
		}

		$results = $stmt->fetchAll(PDO::FETCH_ASSOC);

		return $results ? $results : [];
	} catch (Exception $e) {
		error_log($e->getMessage());
		throw $e;
	}
}

// delete-comment.php
function delete_comment(PDO $pdo, int $post_id, int $comment_id, int $user_id): void
{
	try {
		$query = "DELETE FROM post_comments
                  WHERE id = :comment_id AND user_id = :user_id AND post_id = :post_id";

		$stmt = $pdo->prepare($query);

		if (!$stmt) {
			throw new Exception("Internal server error: " . implode(" ", $pdo->errorInfo()));
		}

		$stmt->bindParam(':comment_id', $comment_id, PDO::PARAM_INT);
		$stmt->bindParam(':post_id', $post_id, PDO::PARAM_INT);
		$stmt->bindParam(':user_id', $user_id, PDO::PARAM_INT);

		if (!$stmt->execute()) {
			throw new Exception("Internal server error: " . implode(" ", $stmt->errorInfo()));
		}
	} catch (Exception $e) {
		error_log($e->getMessage());
		throw $e;
	}
}

// delete-comment.php
function delete_post(PDO $pdo, int $post_id, int $user_id): void
{
	try {
		// deleting post comments
		$query = "DELETE FROM post_comments
                  WHERE post_id = :post_id";
		$stmt = $pdo->prepare($query);
		if (!$stmt)
			throw new Exception("Internal server error: " . implode(" ", $pdo->errorInfo()));
		$stmt->bindParam(':post_id', $post_id, PDO::PARAM_INT);
		if (!$stmt->execute())
			throw new Exception("Internal server error: " . implode(" ", $stmt->errorInfo()));

		// deleting post likes
		$query = "DELETE FROM post_likes
                  WHERE post_id = :post_id";
		$stmt = $pdo->prepare($query);
		if (!$stmt)
			throw new Exception("Internal server error: " . implode(" ", $pdo->errorInfo()));
		$stmt->bindParam(':post_id', $post_id, PDO::PARAM_INT);
		if (!$stmt->execute())
			throw new Exception("Internal server error: " . implode(" ", $stmt->errorInfo()));

		// deleting post
		$query = "DELETE FROM posts
                  WHERE id = :post_id AND owner = :user_id";
		$stmt = $pdo->prepare($query);
		if (!$stmt)
			throw new Exception("Internal server error: " . implode(" ", $pdo->errorInfo()));
		$stmt->bindParam(':post_id', $post_id, PDO::PARAM_INT);
		$stmt->bindParam(':user_id', $user_id, PDO::PARAM_INT);
		if (!$stmt->execute())
			throw new Exception("Internal server error: " . implode(" ", $stmt->errorInfo()));

		// deleting post picture
		$query = "
				DELETE FROM pictures
				WHERE
				waiting_for IS NULL AND id = (
					SELECT picture FROM posts WHERE id = :post_id
				);
		";
		$stmt = $pdo->prepare($query);
		if (!$stmt)
			throw new Exception("Internal server error: " . implode(" ", $pdo->errorInfo()));
		$stmt->bindParam(':post_id', $post_id, PDO::PARAM_INT);
		if (!$stmt->execute())
			throw new Exception("Internal server error: " . implode(" ", $stmt->errorInfo()));
	} catch (Exception $e) {
		error_log($e->getMessage());
		throw $e;
	}
}

// toggle-comment-notify.php
function toggle_comment_notify(PDO $pdo, int $user_id): void
{
	try {
		$stmt = $pdo->prepare("
            UPDATE users
            SET comment_notify = (
            SELECT CASE
                WHEN comment_notify IS NULL THEN 1
                ELSE NULL
            END
            ) WHERE id = :user_id;
        ");

		if (!$stmt) {
			throw new Exception("Internal server error: " . implode(" ", $pdo->errorInfo()));
		}

		$stmt->bindParam(':user_id', $user_id, PDO::PARAM_INT);

		if (!$stmt->execute()) {
			throw new Exception("Internal server error: " . implode(" ", $stmt->errorInfo()));
		}
	} catch (Exception $e) {
		error_log($e->getMessage());
		throw $e;
	} finally {
		if ($stmt !== null) {
			$stmt->closeCursor();
		}
	}
}

// get-oomments.php
function get_comment_notify(PDO $pdo, int $user_id): array
{
	try {
		$query = "SELECT
        COALESCE(comment_notify, 0) AS value
        FROM users
        WHERE id = :user_id";

		$stmt = $pdo->prepare($query);

		if (!$stmt) {
			throw new Exception("Internal server error: " . implode(" ", $pdo->errorInfo()));
		}

		$stmt->bindParam(':user_id', $user_id, PDO::PARAM_INT);

		if (!$stmt->execute()) {
			throw new Exception("Internal server error: " . implode(" ", $stmt->errorInfo()));
		}

		$results = $stmt->fetchAll(PDO::FETCH_ASSOC);

		return $results ? $results[0] : [];
	} catch (Exception $e) {
		error_log($e->getMessage());
		throw $e;
	}
}

// index.php
function get_post(PDO $pdo, int $post_id): array
{
	try {
		$query = "
            SELECT
                posts.picture,
                users.username,
                users.email AS useremail,
                posts.date
            FROM posts
            INNER JOIN users ON posts.owner = users.id
            WHERE posts.id = :post_id;";

		$stmt = $pdo->prepare($query);

		if (!$stmt) {
			throw new Exception("Internal server error: " . implode(" ", $pdo->errorInfo()));
		}

		$stmt->bindParam(':post_id', $post_id, PDO::PARAM_INT);

		if (!$stmt->execute()) {
			throw new Exception("Internal server error: " . implode(" ", $stmt->errorInfo()));
		}

		$r = $stmt->fetch(PDO::FETCH_ASSOC);

		return $r ?: [];
	} catch (Exception $e) {
		error_log($e->getMessage());
		throw $e;
	} finally {
		if ($stmt !== null) {
			$stmt->closeCursor();
		}
	}
}

// update-theme.php
function set_theme(PDO $pdo, int $user_id, string $theme): void
{
	try {
		$stmt = $pdo->prepare("
            UPDATE users
            SET theme = :theme
            WHERE id = :user_id;
        ");

		if (!$stmt) {
			throw new Exception("Internal server error: " . implode(" ", $pdo->errorInfo()));
		}

		$stmt->bindParam(':user_id', $user_id, PDO::PARAM_INT);
		$stmt->bindParam(':theme', $theme, PDO::PARAM_STR);

		if (!$stmt->execute()) {
			throw new Exception("Internal server error: " . implode(" ", $stmt->errorInfo()));
		}
	} catch (Exception $e) {
		error_log($e->getMessage());
		throw $e;
	} finally {
		if ($stmt !== null) {
			$stmt->closeCursor();
		}
	}
}
