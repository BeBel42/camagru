<?php

namespace Layout;

function printFooter($loggedIn = false)
{ ?>
	<footer>
		<hr>
		<nav class='mb-3 d-flex justify-content-center'>
			<?php if (!$loggedIn) : ?>
				<a href='/signup'>Sign up</a>
				<a href='/login'>Log in</a>
			<?php else : ?>
				<a href='/settings?logout=true'>Logout</a>
			<?php endif; ?>
			<a href='/about'>About</a>
			<a href='https://gitlab.com/BeBel42/camagru'>Source code</a>
			<a href='https://mlefevre.xyz'>mlefevre</a>
			<a href='https://campus19.be/'>Campus 19</a>
		</nav>
	</footer>
<?php }

function printHeader($loggedIn = false)
{ ?>
	<nav class='navbar sticky-top navbar-expand-sm bg-secondary py-4'>
		<div class='container'>
			<a style='text-decoration: none;' class='fs-2' href='/' class='navbar-brand'> <b>Camagru</b> </a>
			<ul class='navbar-nav fs-4' style='gap: 20px'>
				<?php if ($loggedIn) : ?>
					<li class='nav-item'>
						<a
							style='text-decoration: none;'
							href='/editor'>Create</a>
					</li>
					<li class='nav-item'>
						<a

							style='text-decoration: none;'
							href='/settings'>Settings</a>
					</li>
					<li class='nav-item'>
						<a

							style='text-decoration: none;'
							href='/settings?logout=true'>Logout</a>
					</li>
				<?php else : ?>
					<li class='nav-item'>
						<a

							style='text-decoration: none;'
							href='/login'>Log in</a>
					</li>
					<li class='nav-item'>
						<a

							style='text-decoration: none;'
							href='/signup'>Sign up</a>
					</li>
				<?php endif; ?>
			</ul>
		</div>
	</nav>
<?php } ?>
