<?php

require_once($_SERVER['DOCUMENT_ROOT'] . '/../src/php/session.php');

if (empty($_SESSION['logged_user'])) {
	header('Location: /login');
	exit();
}
