<?php

$tables = array_reverse([
    'users',
    'pictures',
    'posts',
    'post_comments',
    'post_likes',
    'email_types',
    'sent_emails',
    'cookie_tokens',
    'sessions',
]);



try {
    $env = parse_ini_file('.env');
    $pdo = new PDO(
        "mysql:host={$env['MARIADB_HOST']};port={$env['MARIADB_PORT']};dbname={$env['MARIADB_DATABASE']}",
        $env['MARIADB_USER'], $env['MARIADB_ROOT_PASSWORD']);
    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    foreach ($tables as $table_name)
        $pdo->exec("DROP TABLE $table_name");
} catch (PDOException $e) {
    error_log($e->getMessage());
    die("Database error: " . $e->getMessage());
}
